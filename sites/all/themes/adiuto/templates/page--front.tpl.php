<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
  <div class="<?php print $container_class; ?>">
    <div class="navbar-header">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>

      <?php if (!empty($site_name)): ?>
        <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>

      <?php /*if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
        <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navbar-collapse">
          <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      <?php endif; */?>
    </div>

    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div classxx="navbar-collapse collapse">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
      </div>
    <?php endif; ?>
  </div>

</header>
<?php print render($page['header']); ?>

<div class="main-container <?php print $container_class; ?>">

  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>
  </header> <!-- /#page-header -->

  <div class="row row-offcanvas row-offcanvas-right">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section class="col-sm-12 col-lg-12"><?php //print $content_column_class; ?>
      <?php print $messages; ?>
      <div class="highlighted">
      <?php if (!empty($page['highlighted'])): ?>
        <?php print render($page['highlighted']); ?>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php /*if (!empty($title)): ?>
        <div class="front-matter">
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif;*/ ?>
       <?php if (!empty($node)) : ?>
            <?php $slogan = field_get_items('node', $node, 'field_slogan'); ?>
              <?php if ((!empty($slogan))): ?>
                <h3 class="subtitle slogan"><?php print $slogan[0]['value']; ?></h3>
              <?php endif; ?>
          <?php endif; ?> 
        </div>
      </div>
      <?php print render($title_suffix); ?>
      <?php #print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links) && !count($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <section id="front-node" class="front row">
          <div class="col-md-1"></div>
          <div class="col-md-5">
            <img class="img-responsive" src="<?php print file_create_url($node->field_images['und'][0]['uri']) ?>">
          </div>
          <div class="col-md-5">
            <?php print render($page['content']); ?>
          </div>
          <div class="col-md-1"></div>
      </section>
      <section id="front-tasks" class="front row" style="background: url('/sites/all/themes/bootstrap_adiuto_cdn/background.jpg') center center;">
        <div class="col-xs-12">
          <h2 class="text-center" style="color: transparent"><strong><?php print t('Tasks'); ?></strong></h2>
          <?php
            // Tasks exposed From
            $view = views_get_view('task_search');
            $display_id = 'page'; // Display: page/block
            $view->set_display($display_id);
            $view->init_handlers();
            $form_state = array(
              'view' => $view,
              'display' => $view->display_handler->display,
              'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'),
              'method' => 'get',
              'rerender' => TRUE,
              'no_redirect' => TRUE,
              'always_process' => TRUE, // This is important for handle the form status.
            );
            $form = drupal_build_form('views_exposed_form', $form_state);
            $form['group']['query']['#attributes']['placeholder'] = t('Search tasks');
            #var_dump($form['group']['query']['#attributes']);
            print drupal_render($form);
          ?>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-10 text-center">
          <?php
            // Locations
            print views_embed_view('tasks_locations', 'block');
          ?>
        </div>
        <div class="col-md-1"></div>
      </section>
      <section id="front-initiatives" class="row front" style="background-color:#F3F5F8; " >
        <div class="col-xs-12">
          <?php /*<h2 class=""><strong><?php print t('Initiatives'); ?></strong></h2> */?>
          <?php
            // Render 3 initiatives blocks
            print views_embed_view('initiative_list', 'block_1');
          ?>
        </div>
      </section>
      <section id="front-map" class="row">
        <?php
          // Render initiatives map
          print views_embed_view('initiative_map', 'block_initiatives_map');
        ?>
      </section>
      <section id="front-info" class="row front" style="background-color: #F3F5F8" >
        <div class="col-md-4">
          <div class=" panel panel-default panel-body">
            <article>
              <img class="img-responsive" src="<?php print file_create_url($node->field_images['und'][1]['uri']) ?>"> 
              <?php
                $block = block_load('block', 5);      
                $block_output = _block_get_renderable_array(_block_render_blocks(array($block))); 
                print drupal_render($block_output);
              ?>
            </article>
              <a class="btn-group" href="/tasks"><span class="btn btn-light"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> <?php print t('Tasks'); ?></span><span class="btn btn-success"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></span></a>
              <a class="btn-group" href="/initiatives"><span class="btn btn-light"><span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> <?php print t('Initiatives'); ?></span><span class="btn btn-success"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></span></a>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default panel-body">
            <article>
            <img class="img-responsive" src="<?php print file_create_url($node->field_images['und'][2]['uri']) ?>">
              <?php
                $block = block_load('block', 6);
                $block_output = _block_get_renderable_array(_block_render_blocks(array($block))); 
                print drupal_render($block_output);
              ?>
            </article>
            <a class="btn-group" href="/node/add/initiative"><span class="btn btn-light"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <?php print t('Register a new initiative'); ?></span><span class="btn btn-success"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></span></a>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default panel-body">
            <article>
            <img class="img-responsive" src="<?php print file_create_url($node->field_images['und'][3]['uri']) ?>">
              <?php
                $block = block_load('block', 9);
                $block_output = _block_get_renderable_array(_block_render_blocks(array($block))); 
                print drupal_render($block_output);
              ?>
            </article>
            <a class="btn-group" href="https://www.paypal.com/pools/c/8QdGi39lR3" target="_blank"><span class="btn btn-light"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span> <?php print t('Donate via PayPal'); ?></span><span class="btn btn-info"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></span></a>
          </div>
        </div>
      </section>
      <section id="front-stats" class="front row text-center">
        <?php
          // Render achievements
          print views_embed_view('achievements', 'block_1');
        ?>

        <img class="img-responsive" src="<?php print file_create_url($node->field_images['und'][4]['uri']) ?>">

      </section>
    </section>

    <?php /* if (!empty($page['sidebar_second'])): ?>
      <aside class="col-lg-3 col-sm-4 col-xs-6 sidebar-offcanvas offcanvas row-offcanvas-right" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; */?>

  </div>
</div>

<?php if (!empty($page['footer'])): ?>
  <footer class="<?php print $container_class; ?>">
    <?php print render($page['footer']); ?>
  </footer>
<?php endif; ?>
