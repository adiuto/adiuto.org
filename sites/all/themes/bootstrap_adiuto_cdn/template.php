<?php
/**
 * @file
 * template.php
 */

function bootstrap_adiuto_cdn_preprocess_page(&$variables) {
  $variables['content_column_class'] = ' class="col-sm-8 col-lg-9"';
}


function bootstrap_adiuto_cdn_menu_link(array $variables) {
  $element = $variables ['element'];
  $sub_menu = '';

  if ($element ['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $element['#localized_options']['html'] = 1;
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function bootstrap_adiuto_cdn_commerce_cart_empty_block() {
  return '<div class="">' . t('To plan your Mission, add some tasks.') . '<a href="/help#help-missions" alt="help"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></div>';
}


function bootstrap_adiuto_cdn_preprocess_select_as_checkboxes(&$variables) {
    $element = &$variables['element'];
    //Remove form-control class added to original "select" element
    if (($key = array_search('form-control', $element['#attributes']['class'])) !== false) {
        unset($element['#attributes']['class'][$key]);
    }    
}


/**
 * Allows sub-themes to alter the array used for colorizing text.
 *
 * @param array $texts
 *   An associative array containing the text and classes to be matched, passed
 *   by reference.
 *
 * @see _bootstrap_colorize_text()
 */
function bootstrap_adiuto_cdn_bootstrap_colorize_text_alter(&$texts) {

  // This matches the exact string: "My Unique Button Text".
  $texts['matches'][t('Locate')] = 'info';
  
    // This matches the exact string: "My Unique Button Text".
  $texts['matches'][t('To mission')] = 'success';
  $texts['matches']['<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ' . t('To mission')] = 'success';
  
//  drupal_set_message(print_r($texts['matches'][t('To mission')], true));
  $texts['matches'][t('Abort mission')] = 'danger';
  
  // TODO: This is shit!
  $texts['matches'][t('Neue Nachricht verfassen')] = 'success';
  $texts['matches'][t('Send message')] = 'success';
  $texts['matches'][t('Beitreten')] = 'success';
  
  
//  // Remove matching for strings that matches saved search button:
//  drupal_set_message('button: ' . print_r($texts['matches']['<span class="glyphicon glyphicon-save" aria-hidden="true"></span> ' . t('Save search to givelist')], true));
//  $texts['contains'][t('Save search to givelist')] = 'success';
//  unset($texts['contains'][t('Save search to givelist')]);
//  drupal_set_message(print_r($texts['matches'], true));
////  drupal_set_message('bootstrap, behave yourself');


#  // This would also match the string above, however the class returned would
#  // also be the one above; "matches" takes precedence over "contains".
#  $texts['contains'][t('Unique')] = 'bullhorn';

#  // Remove matching for strings that contain "filter":
#  unset($texts['contains'][t('Filter')]);

#  // Change the icon that matches "Upload" (originally "upload"):
#  $texts['contains'][t('Upload')] = 'ok';
}

function bootstrap_adiuto_cdn_bootstrap_iconize_text_alter(array &$texts) {
  // This matches the exact string: "My Unique Button Text".
  $texts['matches'][t('Save')] = 'ok';

  // This would also match the string above, however the class returned would
  // also be the one above; "matches" takes precedence over "contains".
  #$texts['contains'][t('Givelist')] = 'save';

  // Remove matching for strings that contain "filter":
  unset($texts['contains'][t('Save')]);

  // Change the icon that matches "Upload" (originally "upload"):
  #$texts['contains'][t('Upload')] = 'ok';
}


// for main menu
function bootstrap_adiuto_cdn_preprocess_block(&$vars) {

  /* Set shortcut variables */
  $block_id = $vars['block']->module . '_' . $vars['block']->delta;
  #dsm($block_id);
  /* Add classes based on the block delta */
  switch ($block_id) {
    case 'menu_menu-information':
      $vars['classes_array'][] = 'nav-light';
      break;
  }
}


function bootstrap_adiuto_cdn_preprocess_html(&$variables) {
  if (bootstrap_adiuto_cdn_initiative()) {
    $variables['body_attributes_array']['class'][] = 'context-initiative';
  }
}

function bootstrap_adiuto_cdn_initiative() {
  $patterns = 'initiative/*' . PHP_EOL .
              'node/*/tasks' . PHP_EOL .
              'node/*/tasks/*' . PHP_EOL .
              'node/*/members'
  ;
  $path = current_path();
  $path_alias = drupal_lookup_path('alias', $path);
  if(drupal_match_path($path, $patterns) || drupal_match_path($path_alias, $patterns)) {
    return TRUE;
  }
}

/**
 * Overrides bootstrap_menu_local_tasks().
 */
function bootstrap_adiuto_cdn_menu_local_tasks(&$variables) {
  $output = '';
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    // this is the custom part to change the html see bootstrap_adiuto_cdn_preprocess_page
    // Initiative pages
    if (bootstrap_adiuto_cdn_initiative()) { 
      $variables['primary']['#prefix'] .= '<div class="row"><ul class="tabs--primary nav page-navigation nav-light hidden">';
      $variables['primary']['#suffix'] = '</ul></div>';
    }
    // User pages
    elseif (arg(0) == 'user'){
      $variables['primary']['#prefix'] .= '<ul class="user-navigation nav nav-light row">';
      #$variables['primary']['#prefix'] .= '<li>' . l(t('Dashboard'), 'user/' . arg(1)) . '</li>';
      #$variables['primary']['#prefix'] .= '<li>' . l(t('Edit profile'), 'user/' . arg(1) . '/edit') . '</li>';
      $variables['primary']['#prefix'] .= '<li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">' . t('Navigation') . ' <span class="glyphicon glyphicon-menu-down"></span></a><ul class="dropdown-menu">';
      $variables['primary']['#suffix'] = '<li><a href="/user/logout"><span class="glyphicon glyphicon-off"></span> ' . t('Logout') . '</a></li>';
      $variables['primary']['#suffix'] .= '</ul></li></ul>';
    }
    else {
      $variables['primary']['#prefix'] .= '<div class="row"><ul class="tabs--primary nav page-navigation nav-light">';
      $variables['primary']['#suffix'] = '</div></ul>';
    }
    $output .= drupal_render($variables['primary']);
  }

  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs--secondary pagination pagination-sm">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}


// http://codekarate.com/blog/removing-fieldset-drupal-7-date-field
function bootstrap_adiuto_cdn_date_combo($variables) {
  return theme('form_element', $variables);
}


function bootstrap_adiuto_cdn_facetapi_link_active($variables) {
  // dirty hack! 1 of 3
  $path =  (explode("/",$variables['path']));
  if (isset($path[1]) && $path[1] == '%') {
    $path[1] = arg(1);
    $variables['path'] = implode('/', $path);
  }
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];
  $link_text = (mb_strlen($link_text, 'UTF-8') < 26) ?  $link_text : mb_substr($link_text, 0, 25, 'UTF-8') . '…';
  
  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'], 
    'active' => TRUE,
  );
 
  $variables['text']  = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ' . $link_text;
  #$variables['text'] = '<span class="btn btn-light btn-sm">' . $variables['text']  .'</span>';
  $variables['options']['html'] = TRUE;
  $variables['options']['attributes']['class'][] = 'btn';
  $variables['options']['attributes']['class'][] = 'btn-light';
  $variables['options']['attributes']['class'][] = 'btn-sm';
  $variables['options']['attributes']['class'][] = 'active';
 
  //array_push($variables['options']['attributes']['class'], "list-group-item");
  //array_push($variables['options']['attributes']['class'], "row");
 
  return theme_link($variables);
}

function bootstrap_adiuto_cdn_facetapi_link_inactive($variables) {
  // dirty hack! 2 of 3
  $path =  (explode("/",$variables['path']));
  if (isset($path[1]) && $path[1] == '%') {
    $path[1] = arg(1);
    $variables['path'] = implode('/', $path);
  }
  // Builds accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'], 
    'active' => FALSE,
  );
  $accessible_markup = theme('facetapi_accessible_markup', $accessible_vars);
 
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $variables['text'] = ($sanitize) ? check_plain($variables['text']) : $variables['text'];
  $variables['text'] = (mb_strlen($variables['text'], 'UTF-8') < 26) ?  $variables['text'] : mb_substr($variables['text'], 0, 25, 'UTF-8') . '…';
  $variables['text'] = '<span class="text">' . $variables['text'] . '</span>';
  // Adds count to link if one was passed.
  if (isset($variables['count'])) {
    $variables['text'] .= '<span class="count">' . $variables['count'] . '</span>';
  }
  #$variables['text'] = '<span class="btn btn-light btn-sm">' . $variables['text']  .'</span>';
  // Resets link text, sets to options to HTML since we already sanitized the
  // link text and are providing additional markup for accessibility.
  $variables['text'] .= $accessible_markup;
  $variables['options']['html'] = TRUE;
  $variables['options']['attributes']['class'][] = 'btn';
  $variables['options']['attributes']['class'][] = 'btn-light';
  $variables['options']['attributes']['class'][] = 'btn-sm';
 
  //array_push($variables['options']['attributes']['class'], "list-group-item");
  //array_push($variables['options']['attributes']['class'], "row");

  return theme_link($variables);
}

function bootstrap_adiuto_cdn_facetapi_deactivate_widget($variables) {
  return '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
}


/**
 * Returns HTML for an active facet item.
 *
 * @param $variables
 *   An associative array containing the keys 'text', 'path', and 'options'. See
 *   the l() function for information about these variables.
 *
 * @ingroup themeable
 */
function bootstrap_adiuto_cdn_current_search_link_active($variables) {
  // dirty hack! 3 of 3
  $path =  (explode("/",$variables['path']));
  if (isset($path[1]) && $path[1] == '%') {
    $path[1] = arg(1);
    $variables['path'] = implode('/', $path);
  }
  // Sanitizes the link text if necessary.
  $sanitize = empty($variables['options']['html']);
  $link_text = ($sanitize) ? check_plain($variables['text']) : $variables['text'];
  $link_text = (mb_strlen($link_text, 'UTF-8') < 26) ?  $link_text : mb_substr($link_text, 0, 25, 'UTF-8') . '…';
  $link_text = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ' . $link_text;
  // Theme function variables fro accessible markup.
  // @see http://drupal.org/node/1316580
  $accessible_vars = array(
    'text' => $variables['text'],
    'active' => TRUE,
  );

  $variables['options']['html'] = TRUE;
  $variables['options']['attributes']['class'][] = 'active';
  // Add additional classes to make the items look like the active filters
  $variables['options']['attributes']['class'][] = 'btn';
  $variables['options']['attributes']['class'][] = 'btn-light';
  $variables['options']['attributes']['class'][] = 'btn-sm';
  // return l($variables['text'], $variables['path'], $variables['options']) . $link_text;
  return l($link_text, $variables['path'], $variables['options']);
}

/**
 * Theme function for unified login page.
 *
 * @ingroup themable
 */
function bootstrap_adiuto_cdn_lt_unified_login_page($variables) {
  
  $login_form = $variables['login_form'];
  $register_form = $variables['register_form'];
  $active_form = $variables['active_form'];
  $output = '';

  $output .= '<div class="toboggan-unified ' . $active_form . '">';

  // Create the initial message and links that people can click on.
  $output .= '<div id="login-message" class="hidden">' . t('You are not logged in.') . '</div>';
  $output .= '<div id="login-links">';
  $output .= l(t('I have an account'), 'user/login', array('attributes' => array('class' => array('login-link', 'btn'), 'id' => 'login-link')));
  $output .= ' ';
  $output .= l(t('I want to create an account'), 'user/register', array('attributes' => array('class' => array('login-link', 'btn'), 'id' => 'register-link')));

  $output .= '</div>';

  // Add the login and registration forms in.
  $output .= '<div id="login-form">' . $login_form . '</div>';
  $output .= '<div id="register-form">' . $register_form . '</div>';

  $output .= '</div>';

  return $output;
}
/**
 * Drupal messages
 */
function bootstrap_adiuto_cdn_status_messages($variables) {
  $display = $variables['display'];
  $output = '';
  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {

    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>";
    }
    //if (count($messages) > 1) {
      foreach ($messages as $message) {
        $output .= '<div class="clearfix bs-callout messages bs-callout-' . $type . '">';
        $output .= $message;
        $output .= '</div>';
      }
    //}
    //else {
    //  $output .= reset($messages);
    //}

  }
  return $output;
}


/**
 * Returns HTML for a list or nested list of items.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: An array of items to be displayed in the list. If an item is a
 *     string, then it is used as is. If an item is an array, then the "data"
 *     element of the array is used as the contents of the list item. If an item
 *     is an array with a "children" element, those children are displayed in a
 *     nested list. All other elements are treated as attributes of the list
 *     item element.
 *   - title: The title of the list.
 *   - type: The type of list to return (e.g. "ul", "ol").
 *   - attributes: The attributes applied to the list element.
 */
function bootstrap_adiuto_cdn_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '';//'<div class="item-list">';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  #$output .= '</div>';
  return $output;
}