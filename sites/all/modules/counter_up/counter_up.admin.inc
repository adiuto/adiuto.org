<?php

/**
 * @file
 * This file provides the configuration form for the Counter Up module.
 */

/**
 * Implements hook_form().
 */
function counter_up_form($form, $form_state) {

  $form['library'] = array(
    '#type' => 'fieldset',
    '#title' => t('Counter Up Library'),
  );

  if (function_exists('libraries_detect')) {
    $info = libraries_detect('counterup');
    if ($info['installed'] == FALSE) {
      drupal_set_message(t('counterup library is not installed. Download it from <a href="@source-url" target="_blank">the official page</a>. This module expects the library to be at sites/all/libraries/counterup/jquery.counterup.min.js, along with other .js files.', array('@source-url' => 'https://github.com/bfintal/Counter-Up')), 'error');
    }
    else {
      $form['library']['#description'] = t('counterup library is installed. <strong>Version detected: @version</strong>.<p>After changing these settings, flush caches.</p>', array('@version' => $info['version']));
    }
  }

  $form['counter_up_delay'] = array(
    '#title' => t('Delay Time'),
    '#type' => 'textfield',
    '#description' => t('The delay in milliseconds per number count up.'),
    '#default_value' => variable_get('counter_up_delay', 10),
  );
  $form['counter_up_time'] = array(
    '#title' => t('Time'),
    '#type' => 'textfield',
    '#description' => t('The total duration of the count up animation.'),
    '#default_value' => variable_get('counter_up_time', 1000),
  );

  return system_settings_form($form);
}