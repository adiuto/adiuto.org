<?php

/**
* Implementation of hook_rules_action_info().
*/
function custom_misc_rules_action_info() {
  return array(
  'send_first_task_added_message' => array(
    'label' => t('Sends a message when first task is added'),
    'group' => 'Custom misc',
    'base' => 'rules_action_send_first_task_added_message',
    'parameter' => array(
        'group_name' => array(
          'type' => '*',
          'label' => t('Initiative name'),
          'description' => t('The name of an initiaitve'),
        ),
        'group_path' => array(
          'type' => '*',
          'label' => t('Initiative path'),
          'description' => t('The url path of an initiaitve'),
        ),
        'group_id' => array(
          'type' => '*',
          'label' => t('Initiative ID'),
          'description' => t('The ID of an initiaitve'),
        ),          
        'task_name' => array(
          'type' => '*',
          'label' => t('Task title'),
          'description' => t('The name of a task'),
        ),        
      ),
    ),
  'send_last_task_removed_message' => array(
    'label' => t('Sends a message when last task was removed'),
    'group' => 'Custom misc',
    'base' => 'rules_action_send_last_task_removed_message',
   ),
  'add_to_mission_message' => array(
    'label' => t('Dispalys a message when a task was added to a mission'),
    'group' => 'Custom misc',
    'base' => 'rules_action_add_to_mission_message',
    'parameter' => array(
        'group_name' => array(
          'type' => '*',
          'label' => t('Initiative name'),
          'description' => t('The name of an initiaitve'),
        ),
        'group_path' => array(
          'type' => '*',
          'label' => t('Initiative path'),
          'description' => t('The url path of an initiaitve'),
        ),
        'group_id' => array(
          'type' => '*',
          'label' => t('Initiative ID'),
          'description' => t('The ID of an initiaitve'),
        ),              
        'task_name' => array(
          'type' => '*',
          'label' => t('Task title'),
          'description' => t('The name of a task'),
        ),
        'task_id' => array(
          'type' => '*',
          'label' => t('Task ID'),
          'description' => t('The ID of a task'),
        ),                
      ),
    ),
  'load_og_role' => array(
    'label' => t('Load an OG role by rid and returns it\'s name'),
    'group' => 'Custom misc',
    'base' => 'rules_action_load_og_role',
    'provides' => array(
      'og_role_name' => array('type' => 'text', 'label' => t('OG role name')),
    ),    
    'parameter' => array(
        'rid' => array(
          'type' => '*',
          'label' => t('RID'),
          'description' => t('An organic groups role ID'),
        ),
      ),
    ),    
  );
}

/**
 * Action:
 */
function rules_action_load_og_role($rid) {
  return array('og_role_name' => og_role_load($rid)->name);
}

/**
 * Action: first task added to mission
 */
function rules_action_send_first_task_added_message($group_name, $group_path, $group_id, $task_name) {
  $msg = t("<h4>Task added,</h4>%task from initiaitve %initiative added to your mission.</br>You can add more tasks for %initiative to your mission, or view your mission.", 
             array(
    '%task'       => $task_name,
    '%initiative'       => $group_name,
    '@url_finish' => "/checkout",
    '@url_initiative' =>  $group_path,
    '@url_tasks' =>  $group_path . "/tasks",
    '!help-missions' => custom_help_link("donor-how-to#mission"),
  ));
  
  // Shorten the string, but respect word boundaries 
  if (strlen($group_name) > 32 && preg_match('/^.{1,32}\b/su', $group_name, $match)) {
    $group_name = $match[0] . "…";
  }  

  $footer = l(t('Mission'), "/cart", array('html' =>true, 'attributes' => array('class' => 'btn btn-sm btn-warning')));
  $footer .= l(t("More tasks for") . ' ' . $group_name, "/node/$group_id/tasks", array('html' =>true, 'attributes' => array('class' => 'btn btn-sm btn-success')));

  $markup  =  $msg . '<div class="bs-footer">' . $footer . '</div>';
  drupal_set_message($markup, 'status', false);
}

/**
 * Action: Last tasks removed from mission
 */
function rules_action_send_last_task_removed_message() {
  $msg = t("There are no more tasks in your mission.", 
             array(
    '@any_task_url' =>  "/tasks",
    '!help_missions' => custom_help_link("donor-how-to#mission"),
  ));
  
  drupal_set_message($msg, 'status', false);
}

/**
 * Action: Adding tasks to mission
 */
function rules_action_add_to_mission_message($group_name, $group_path, $group_id, $task_name, $task_id) {
  
  $task  =  entity_load_single('commerce_product', $task_id);

  // Shorten the string, but respect word boundaries 
  $group_name_short = (strlen($group_name) > 32 && preg_match('/^.{1,32}\b/su', $group_name, $match)) ? ($match[0] . "…") : $group_name;

  // Send message only if the added task doesn't conflict with the initiaitve
  // of the current order.  
  if (og_is_member('node', $group_id, 'commerce_product', $task)) {
    $msg = t("%task_name added to your mission.",
      array(
        '%task_name' =>  $task_name,
      ),
    );
    $message_status = "status";
  } else {
    $msg = t("<h4>Sorry,</h4>%task_name <br>could not be added to your mission! Your current mission is for %initiative_name, only tasks from this initiative can be added to your mission.",
               array(
      '@mission_url'  =>  "/cart",
      '@ceckout_url'  =>  "/ceckout",
      '%task_name' =>  $task_name,
      '%initiative_name' =>  $group_name,
      '@initiative_url' =>  "/node/$group_id",
      '!help_missions' => custom_help_link("donor-how-to#mission", 'btn-link btn-xs'),
    ));

    // this could be modal if we added modal-message after warning its just a class getting added to the message
    $message_status = "warning modal-message";  
  }

  $footer = l(t('Mission'), "/cart", array('html' =>true, 'attributes' => array('class' => 'btn btn-sm btn-warning')));
  $footer .= l(t("Tasks for") . ' ' . $group_name_short, "/node/$group_id/tasks", array('html' =>true, 'attributes' => array('class' => 'btn btn-sm btn-success')));

  $markup  =  $msg . '<div class="bs-footer">' . $footer . '</div>';
  drupal_set_message($markup, $message_status, false);
}
