<?php
/**
 * Export Drupal Commerce products to Views.
 */

/**
 * Implements hook_views_data()
 */
function custom_misc_views_data_alter(&$data) {

#  $data['commerce_product_task']['table']['group']  = t('Custom misc');

#  $data['commerce_product_task']['table']['base'] = array(
#    'field' => 'product_id',
#    'title' => t('Commerce Product'),
#    'help' => t('Products from the store.'),
#  );
#  $data['commerce_product_task']['table']['entity type'] = 'commerce_product';


  $data['commerce_product']['operations'] = array(
    'field' => array(
      'title' => t('Custom Task operations links'),
      'help' => t('Display all the available operations links for the task.'),
      'handler' => 'custom_misc_handler_field_product_operations',
    ),
  );

  return $data;
}

