
//jQuery(document).ready(function($) {
(function ($) {
$(document).ready(function() {
  
function isEllipsisActive($jQueryObject) {
    return ($jQueryObject.outerWidth() < $jQueryObject[0].scrollWidth);
}
  
  $('.block-facetapi li').each(function(i) {
    $(this).css({ 
      'overflow': 'hidden',
      'max-width': '100%'
    })
    var test = isEllipsisActive($(this));
    if (test) {
      $(this).css({ 
        color: 'red',
      })
    }
  });
  
  // Hide cart (your misson) from main menu if cart ist empty.
  // And appends counter to cart showing the numbers of tasks you took over.
  function cartMenu() {
    // Get the commerce cart table in the right pane an count its rows.
    let taskCount = $('.view-commerce-cart-block table tr').length;
    
    // Get all entries in mobile and desktop main menu.
    var menues = [
      $('#navbar .menu li'),
      $('#block-system-main-menu .menu li')
    ];
    for (let menu of menues) {
      menu.each(function(idy, li) {
        var li = $(li);
        var a = li.find("a");

        // The menu item for the missons should be hidden if cart is empty.
        // If not we add a counter of cart items.
        if (a.attr("href") == "/cart") {
          if (!taskCount) {
           li.hide();
          } else {
            var placeholder = a.find(".cart-count-placeholder");
            var badge = $(document.createElement('div'))
                          .html(taskCount)
                          .addClass("badge alert-warning cart-count")
                          .appendTo(placeholder)
                          ;
          }
        }
      });
    }
  }
  
  // Add button classes to radios. It's not possible to do it with form API or
  // with theme hooks.
  function transformRadiosToButtons() {
    var label   = $('.form-type-radio .control-label');
    var wrapper = $('div.form-type-radio');
    
    var facetLabel = $('.block-facetapi .control-label');
    var facetWrapper = $('.block-facetapi div.form-type-checkbox');
    
    // Add classes to label.
    label.addClass('btn btn-light btn-sm');
    facetLabel.addClass('btn btn-light btn-sm');

    // Add classes to div wrapper.
    wrapper.attr('data-toggle', 'buttons');
    facetWrapper.attr('data-toggle', 'buttons');
    
    // Add extra small button class to all widgets.
    $('.views-exposed-widget .form-type-radio label, #edit-proximity .form-type-radio label').addClass('btn-xs');
    $('.block-facetapi .control-label').addClass('btn-xs');
  }

  // Toggle active class for all radios.
  function toggleActive() {
    var radio = $('input[type="radio"],[type=checkbox]');
    radio.each(function () {
      if ($(this).is(':checked')) {
        $(this).parent().each(function () {
          $(this).addClass('active');
          $(this).addClass('btn-success');
        });
      } else {
        $(this).parent().each(function () {
          $(this).removeClass('active');
          $(this).removeClass('btn-success');
        });
      }
    });
  }
  
  // Trigger toggle on document ready.
  transformRadiosToButtons();
  toggleActive();
  cartMenu();
  
  // Trigger toggle on status change.
  var radio = $('input[type="radio"]');
  radio.change(function() {
    toggleActive();
  });
  
  Drupal.behaviors.YourBehaviour = {
  attach: function(context, settings) {  
    transformRadiosToButtons();
    toggleActive();
   }
  } 

});  
})(jQuery);
