<?php

/**
 * Field handler to present a product's operations links.
 */
class custom_misc_handler_field_product_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['product_id'] = 'product_id';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['add_destination'] = TRUE;

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['add_destination'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add a destination parameter to operations links so users return to this View on form submission.'),
      '#default_value' => $this->options['add_destination'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $product_id = $this->get_value($values, 'product_id');
    $product_status = isset($values->commerce_product_status) ? (int) $values->commerce_product_status : false;

    // Get the operations links.
    $options = array('html' => TRUE, 'attributes' => array('class' => ''));
    $links['edit'] = array(
      'title' => '<small><span class="glyphicon glyphicon-edit" aria-hidden="true"></span><span class="hidden-xs hidden-sm hidden-md"> ' . t('Edit') .     '</span></small>',
      'href' => 'task/' . $product_id . '/edit',
      'html' => TRUE,
      'attributes' => array('class' => array('btn btn-xs btn-default'), 'title' => t('Edit')),
    );
    
    // Link to activate/deactivate can only be shown if we know the current status
    // of the product. We have to avoid to load the entity
    if ($product_status === 0) {
      $links['activate'] = array(
        'title' => '<small><span class="glyphicon glyphicon-check" aria-hidden="true"></span><span class="hidden-xs hidden-sm hidden-md"> ' . t('Activate') . '</span></small>',
        'href' => 'task/' . $product_id . '/activate',
        'html' => TRUE,
        'attributes' => array('class' => array('btn btn-xs btn-success'), 'title' => t('Activate')),
      );
    } elseif ($product_status === 1) {
      $links['disable'] = array(
        'title' => '<small><span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span><span class="hidden-xs hidden-sm hidden-md"> ' . t('Disable') . '</span></small>',
        'href' => 'task/' . $product_id . '/disable',
        'html' => TRUE,
        'attributes' => array('class' => array('btn btn-xs btn-warning'), 'title' => t('Disable')),
      );
    }
    
    $links['delete'] = array(
      'title' => '<small><span class="glyphicon glyphicon-trash" aria-hidden="true"></span><span class="hidden-xs hidden-sm hidden-md"> ' . t('Delete') .   '</span></small>',
      'href' => 'task/' . $product_id . '/delete',
      'html' => TRUE,
      'attributes' => array('class' => array('btn btn-xs btn-danger'), 'title' => t('Delete')),
    );            

    
    if (!empty($links)) {
      // Add the destination to the links if specified.
      if ($this->options['add_destination']) {
        foreach ($links as $id => &$link) {
          $link['query'] = drupal_get_destination();
        }
      }

      #drupal_add_css(drupal_get_path('module', 'commerce_product') . '/theme/commerce_product.admin.css');
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations task-operations'))));
    }
  }
}
