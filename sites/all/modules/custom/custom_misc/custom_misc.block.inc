<?php
/**************************************************
 *                                    Custom Blocks
 **************************************************/

/**
 * Implements hook_block_info().
 */
function custom_misc_block_info() {
  $blocks['custom_misc_initiative_menu'] = array(
    'info' => t('Custom: Initiative menu'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_misc_user_initiatives'] = array(
    'info' => t('Custom: User Initiatives'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_misc_user_menu'] = array(
    'info' => t('Custom: User menu'),
    'cache' => DRUPAL_CACHE_PER_USER,
  );
  $blocks['custom_misc_login'] = array(
    'info' => t('Custom: Login'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  $blocks['custom_misc_add_initiative'] = array(
    'info' => t('Custom: Add initiaitve'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  $blocks['custom_misc_member_status'] = array(
    'info' => t('Custom: Shows user\'s Member status'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_misc_breadcrumbs'] = array(
    'info' => t('Custom: Breadcrumbs'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  $blocks['custom_misc_frontpage_header'] = array(
    'info' => t('Custom: Header of frontpage'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_misc_frontpage_geo'] = array(
    'info' => t('Custom: Geolocation block for frontpage'),
    'cache' => DRUPAL_NO_CACHE,
  ); 
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_misc_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'custom_misc_initiative_menu':
      $block['subject'] = t('Menu');
      $block['content'] = custom_misc_initiative_menu();
      break;
    case 'custom_misc_user_menu':
      $block['subject'] = t('User');
      $block['content'] = custom_misc_user_menu();
      break;
    case 'custom_misc_user_initiatives':
        $block['subject'] = t('My initiatives');
        $block['content'] = custom_misc_user_initiatives();
        break;
    case 'custom_misc_login':
      $block['subject'] = t('Login');
      $block['content'] = custom_misc_login();
      break;     
    case 'custom_misc_add_initiative':
      $block['content'] = custom_misc_add_initiative();
      break;
    case 'custom_misc_member_status':
      $block['content'] = custom_misc_member_roles();
      break;
    case 'custom_misc_breadcrumbs':
      $block['content'] = custom_misc_breadcrumbs();
      break;
    case 'custom_misc_frontpage_header':
      $block['content'] = custom_misc_frontpage_header();
      break;
    case 'custom_misc_frontpage_geo':
      $block['content'] = custom_misc_frontpage_geolocations();
      break;         
  }
  return $block;
}

function custom_misc_frontpage_geolocations() {
  
  // Add geolocation block to frontpage.
  $block = module_invoke('custom_geolocation', 'block_view', 'custom_geolocation_geolocation');
  $block['content']['location']['address']['#attributes']['class'][] = "input-sm";
  
  $markup = render($block);
  return $markup;
}

function custom_misc_frontpage_header() {
  
  $markup ="";
  $options = array('html' => TRUE, 'attributes' => array('class' => ''));
  $item_classes = array('header-menu', 'h3', 'col-sm-3', 'text-center', 'navbar-link');
  $items = array();
  $items[] = array(
    'data' => l('<span class="h4"><span class="glyphicon glyphicon-check" aria-hidden="true"></span></span> ' . t('Tasks') .
              '</br><small>' . t('What is needed?') . '</small>'    , '/tasks', $options),
    'class'=> $item_classes,
  );
  
  $items[] = array(
    'data' => l('<span class="h4"><span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span></span> ' . t('Initiatives') .
              '</br><small>' . t('Who is coordinating?') . '</small>'    , '/initiatives', $options),
    'class'=> $item_classes,
  );
  
  $items[] = array(
    'data' => l('<span class="h4"><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></span> ' . t('Help') .
              '</br><small>' . t('How does it work?') . '</small>'    , '/help', $options),
    'class'=> $item_classes,
  );  
  
  global $user;
  if ($user->uid) {
    $items[] = array(
      'data' => l('<span class="h4"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span> ' . t('Account') .
                '</br><small>' . t('Profile & preferences') . '</small>'    , '/user', $options),
      'class'=> $item_classes,
    ); 
  } else {
    $items[] = array(
      'data' => l('<span class="h4"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span> ' . t('Login / Register') .
                '</br><small>' . t('Join the network!') . '</small>'    , '/user', $options),
      'class'=> $item_classes,
    ); 
  }
  
  $attributes = array('class' => array('menu', 'nav'));
  return $markup . theme_item_list(array('items' => $items, 'title' => "", 'type' => 'ul', 'attributes' => $attributes));
}

/**
 * Returns a link to add an initiaitve
 */
function custom_misc_add_initiative() {
  $options = array('html' => TRUE, 'attributes' => array('class' => 'btn btn-success'));
  $glyph = '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>';
  $link = l("$glyph " . t('Register a new initiative'), '/node/add/initiative', $options);

  $markup = $link . custom_help_link('initiative-how-to-register', 'btn-success');;
  return '<div class="btn-group pull-right" style="margin-bottom: 10px">' . $markup . '</div>';
}


function custom_misc_preprocess_block(&$variables) {
  if ($variables['block']->module == 'custom_misc' && $variables['block']->delta == 'custom_misc_add_initiative') {
    //$variables['classes_array'][] = drupal_html_class('voffset3');
  }
}

/**
 * Returns the member status of current user on group page
 */
function custom_misc_member_roles() {
  $gid  = arg(1);
  global $user;
  
  if (!og_is_member('node', $gid, 'user', null, array(OG_STATE_PENDING, OG_STATE_ACTIVE))) {
    return;
  }
  
  $roles = og_get_user_roles("node", $gid);
  
  // Don't show simple membership if higher role available
  $roles  = count($roles) > 1? array_diff($roles, array("member")): $roles; 
  
  // Plural or singular
  $text = count($roles) > 1? t("Your roles: ") : t("Your role: ");
  
  // Look for status pending if no roles has been found
  if (count($roles) && reset($roles) == "non-member") {
    $node = node_load($gid);
    $pending_members = og_get_group_members_properties($node, array(), 'members__2', 'node');
    $roles = in_array($user->uid, $pending_members) ? array(t('pending')) : array();
    $text  = t("Your membership is");
  }

  foreach ($roles as $role) {
    $link  = custom_help_link('initiative-administration/#members', 'btn-link btn-xs');
    $text .= " <em>$role$link</em> ";
  }
  
  $markup = "<div class='user-role pull-right small'>$text</div>";
  return $markup;
}

/**
 * Returns a menu with links related to an initiative 
 */
function custom_misc_initiative_menu() {
  $nid = custom_misc_get_og_group_reference();
  if ($nid) {
    global $user;
    $items = array();
    if ($user->uid) {
      
      $gid = $nid;
      $node = node_load($nid); // can we replace this with drupal_get_title? No, we can't as we need the node in 314 and later.
    
      if (!$node) {
        return;
      }
      $title = $node->title;
      
      // Shorten the string, but respect word boundaries 
      if (strlen($title) > 32 && preg_match('/^.{1,32}\b/su', $title, $match)) {
        $title = $match[0] . "…";
      }
      
      $options = array('html' => TRUE, 'attributes' => array('class' => array()));
      $items   = array();
      
      // If user is a member of the initiative from the current page.
      if (og_is_member('node', $nid)) {
        // Create task.
        // We want to show the created task after adding a new task, its is important, that initiative members who can create tasks also have the permission to see any (deactivated) task.
        $task_options = array(
          'query' => array(
              'og_group_ref' => $nid,
              'destination' => "node/$nid/task-coordination",
            ), 
          'attributes' => array('class' => array()),
          'html' => TRUE,
        );
        $items[] = l('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ' . t('Add a task'), '/admin/commerce/products/add/product', $task_options);
        // Bulk add tasks with CSV etc.
        $items[] = l('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ' . t('Add multiple tasks'), 'node/' . $nid . '/add-multiple', $task_options);        
        // Coordinate tasks (activate, deactivate, edit delete).
        // Only group roles with the permission 'update any task content' can coordinate tasks.
        if (og_user_access('node', $gid, "update any task content")) {
                 
          // Use views to load pending tasks: This is important, as pending tasks need immediate action. 
          #$pending_tasks = views_get_view_result('initiative_products', 'pending_tasks', $gid);   
          
          // Count tasks with deadline close to end (2 Days)

          $results = db_query("SELECT commerce_product.product_id AS product_id
          FROM {commerce_product} commerce_product
          LEFT JOIN {og_membership} og_membership_commerce_product ON commerce_product.product_id = og_membership_commerce_product.etid
          LEFT JOIN {field_data_field_deadline} field_data_field_deadline ON commerce_product.product_id = field_data_field_deadline.entity_id AND field_data_field_deadline.deleted = '0'
          WHERE (( (og_membership_commerce_product.gid = :gid ) )AND(( (commerce_product.status = '1') AND (field_data_field_deadline.field_deadline_value < NOW() + INTERVAL 2 DAY ))))", array(':gid' => $gid));
          // NOW() + INTERVAL 1 DAY
          $count = $results->rowCount();

          // Create HTML for badge.
          $badge = $count ? '<span title="'.t("Ending @entity", array('@entity' => t('tasks')))
                                                            .'" class="badge pull-right progress-bar-warning">'. $count .'</span>' : "";
          $items[] = l('<span class="glyphicon glyphicon-check" aria-hidden="true"></span> ' . 
                       t('Coordinate tasks') . $badge, 'node/' . $nid . '/task-coordination', $options);
        }
        // Coordinate P2P nodes.
        if (og_user_access('node', $gid, "edit create p2p content")) {

          $items[] = l('<span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> ' . 
                       t('Coordinate P2P'), 'node/' . $nid . '/p2p', $options);
        }
        
        // Coordinate mission (set mission status to accomplished).
        // There are two permissions related to this action: 'set own task status' and 'set any task status'.
        if (og_user_access('node', $gid, "set own task status") || og_user_access('node', $gid, "set any task status")) {
          // Pending missions
          $results = db_query("SELECT commerce_order.order_id
            FROM {commerce_order} commerce_order
            LEFT JOIN {og_membership} og_membership ON commerce_order.order_id = og_membership.etid
            WHERE (( (commerce_order.type IN  ('commerce_order')) AND (commerce_order.status IN  ('pending', 'processing')) AND (og_membership.gid = :gid) ))", array(':gid' => $gid));
          $count = $results->rowCount();
          // Create HTML for badge.
          $badge = ($count) ? '<span title="' . t("Pending @entity", array('@entity' => t('missions'))) . '" class="badge pull-right progress-bar-info">' . $count . '</span>' : '';
          $items[] = l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ' . 
                      t('Coordinate missions') . $badge, 'node/' . $nid . '/mission-coordination', $options);
        }
        
        // Manage Groups.
        if (og_user_access('node', $gid, "administer group") || og_user_access('node', $gid, "approve and deny subscription ")) {
          // Badge for pending members.
          $pending_members = og_get_group_members_properties($node, array(), 'members__2', 'node');
          // Create HTML for badge.
          $badge = count($pending_members) ? '<span title="'.t("Pending @entity", array('@entity' => t('members')))
                                                            .'" class="badge pull-right progress-bar-warning">'.count($pending_members).'</span>' : "";
          $items[] = l('<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ' . t('Administer members') . $badge, 'node/' . $nid . '/members/manage', $options);        
          // Edit Initiative
          $url = 'node/' . $nid . '/edit';
          $items[] = l('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> ' . t('Edit initiative'), $url, $options);
          // Translate
          $url = 'node/' . $nid . '/translate';
          $items[] = l('<span class="glyphicon glyphicon-globe" aria-hidden="true"></span> ' . t('Translate initiative'), $url, $options);
        
        }
        
        // Send private message to group if user has permissions.
        if (custom_misc_privatemsg_group_link()) {
          $items[] = l('<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> ' . 
                       t('Send message to group'), custom_misc_privatemsg_group_link(), $options);       
        }
        
        // Send private message to board members.
        if (custom_misc_privatemsg_group_link(array('coordinator', 'administrator'), false)) {
          $items[] = l('<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> ' . 
                       t('Send message to board members'), custom_misc_privatemsg_group_link(array('coordinator', 'administrator'), false), $options);       
        }
        // Leave initiative
        $items[] = l('<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> ' . 
                      t('Leave the initiative'), '/group/node/' . $nid . '/unsubscribe', $options);
      }
      // If user is not a member a join initiative link is displayed.
      else {
        // Look for status pending if no roles has been found.
        $pending_members = og_get_group_members_properties($node, array(), 'members__2', 'node');
        $pending = in_array($user->uid, $pending_members) ? t('Pending membership') : "";
        
        // Don't show join link, if membership is pending.
        if ($pending) {
          $items['account'] = "<span class='text-info'><span class='glyphicon glyphicon-log-in' aria-hidden='true'></span> $pending</span>";
        } else {
          $items['account'] = l('<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> ' .
                              t('Join the initiative'), '/group/node/' . $nid . '/subscribe', $options);
        }
      }
    return theme_item_list(array('items' => $items, 'title' => $title, 'type' => 'ul', 'attributes' => array('class' => array('nav', 'nav-light'))));
    }
  }
}


/**
 * Returns a menu with links related to the user account 
 */
function custom_misc_user_menu() {
  global $user;
  if ($user->uid) {
  #dsm($user);
    // Unread messages
    $msg_badge   = function_exists('privatemsg_unread_count') && privatemsg_unread_count()?
                   '<span title="'.t("New @entity", array('@entity' => 'messages')).'" class="badge pull-right progress-bar-warning">'.privatemsg_unread_count() .'</span>' : "";
    
    // Show pending orders
    $orders = commerce_order_load_multiple(array(), array('uid' => $user->uid, 'status' =>'pending'));
    $order_badge = count($orders) ? '<span title="'.t("Pending @entity", array('@entity' => t('missions'))).'" class="badge pull-right progress-bar-warning">'.count($orders).'</span>' : "";
    $tasks_pending = views_get_view_result('shop', 'page_user_pending_tasks', $user->uid);
    $tasks_active = views_get_view_result('shop', 'page_user_active_tasks', $user->uid);
    $task_badge_pending = count($tasks_pending) ? '<span title="'.t("Pending @entity", array('@entity' => t('tasks'))).'" class="badge pull-right progress-bar-warning">'.count($tasks_pending).'</span>' : "";
    $task_badge_active  = count($tasks_active)  ? '<span title="'.t("Active @entity", array('@entity' => t('tasks'))).'" class="badge pull-right progress-bar-success">'.count($tasks_active).'</span>' : "";
    $options = array('html' => TRUE, 'attributes' => array('class' => array()));
    
    $items = array(
      l('<span class="glyphicon glyphicon-user" aria-hidden="true"></span> ' . t('My account'), 'user/' . $user->uid, $options),
      l('<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> ' . t('My messages') . " $msg_badge", 'messages', $options),
      l('<span class="glyphicon glyphicon-check" aria-hidden="true"></span> ' . t('My tasks') . " $task_badge_pending $task_badge_active", 'user/' .$user->uid. '/tasks', $options),
      l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ' . t('My missions') . " $order_badge", 'user/' . $user->uid . '/missions', $options),
      l('<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> ' . t('My givelist'), 'user/' . $user->uid . '/givelist', $options),
      l('<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ' . t('My settings'), 'user/' . $user->uid . '/edit', $options),
    );

    $items[] = l('<span class="glyphicon glyphicon-off" aria-hidden="true"></span> ' . t('Logout'), '/user/logout', $options);
    // if we want to add classes to the li's we need to put them in arrays'
#    foreach ($items as $key => $item) {
#      $items[$key] = array(
#        'data' => $item,
#        'class' => array('list-group-item'),
#      );
#    }
    $username = check_plain(format_username($user));
    $links = theme_item_list(array('items' => $items, 'title' => $username, 'type' => 'ul', 'attributes' => array('class' => 'nav nav-light')));
    #return '<div class="list-group">' . implode('', $items) . '</div>';
    return $links;
  }
}

/**
 * Returns a menu with links to user’s initiatives. 
 */
function custom_misc_user_initiatives() {
  return '';
  global $user;
  $options = array('html' => TRUE, 'attributes' => array('class' => ''));
  if ($user->uid) {
    $initiatives = views_get_view_result('user_initiatives', 'default', $user->uid);
    foreach ($initiatives as $i) {
      $items[] = l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $i->node_title, drupal_get_path_alias('node/' . $i->nid), $options);
    }
    $links = theme_item_list(array('items' => $items, 'type' => 'ul', 'attributes' => array('class' => array('nav',  'nav-light'))));
    return $links;
  }
}

function custom_misc_login() {
    $options = array(
      'query' => array(
        'destination' => current_path(),
        'destination_error' => current_path(),
        ),
      'html' => TRUE,
      'attributes' => array('class' => 'btn-xs'),
    );
    $items = array();
    $items[] = l('<span class="glyphicon glyphicon-user" aria-hidden="true"></span> ' . t('Login / Register'), '/user', $options);
    $items[] = l('<span class="custom-icons icons-sm facebook"></span> ' . t('Login with') . ' Facebook', '/hybridauth/window/Facebook', $options);
    $items[] = l('<span class="custom-icons icons-sm twitter"></span> ' . t('Login with') . ' Twitter', '/hybridauth/window/Twitter', $options);
    $items[] = l('<span class="custom-icons icons-sm google"></span> ' . t('Login with') . ' Google', '/hybridauth/window/Google', $options);
    //reset classes for nav

    return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ul', 'attributes' => array('class' => 'nav')));
}


/**
 * Returns a link to send a private message to a group
 * @roles An array with names of group roles. If set only users with given
 *        role will be included. If not all members of the group are included
 *  
 */
function custom_misc_privatemsg_group_link($roles = array('all'), $restriction = true) {
  
  // Private message groups installed?
  if (!module_exists('privatemsg_groups')) {
    return false;
  }
  
  $gid = custom_misc_get_og_group_reference();
  $node = node_load($gid);
  global $user;
  
  // Has permissions?
  if (!og_user_access("node", $gid, 'write privatemsg to group') && $restriction) {
    return false;
  }

  $members = og_get_group_members_properties($node, array(), 'members__1', 'node');
  $recipients = array();

  foreach($members as $uid) {
    $member_roles = og_get_user_roles("node", $gid, $uid);
    foreach($roles as $role) {
      if (($role == "all" or in_array($role, $member_roles)) and 
          $user->uid != $uid)
      $recipients[] =  user_load($uid);
    }
  }
  return privatemsg_get_link($recipients);
}

/**
 * Create custom breadcrumbs
 */
function custom_misc_breadcrumbs() {
  // Add Task - for the commerce admin pages
  if(arg(1) == 'commerce' && arg(2) == 'products') {
    $nid = custom_misc_get_og_group_reference();
    if ($nid) {
      $node = node_load($nid);
      $title = $node->title;
      $destination = drupal_get_destination()['destination'];
      $items = array(
        l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title, 'node/' . $nid, array('html' => TRUE)),
        l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ' . t('Coordinate tasks'), $destination, array('html' => TRUE)),
        t('Create task'),
      );
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    }
  }

  if (is_numeric(arg(1))) {
    // Single Mission
    if (arg(0) == 'mission') {
      $destination = drupal_get_destination()['destination'];
      $path = explode('/', $destination);
      if ($path[0] == 'user') {
        $items = array(
          l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . t('My account'), 'user/' . $path[1], array('html' => TRUE)),
          l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ' . t('My missions'), $destination, array('html' => TRUE)),
          drupal_get_title(),
        );
      }
      else {
        $result = db_query('SELECT n.title, n.nid FROM {node} n JOIN {og_membership} o ON o.gid=n.nid WHERE o.entity_type = \'commerce_order\' AND o.etid = :id', array(':id' => arg(1)));
        $record = $result->fetchAssoc();
        $items = array(
          l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $record['title'], 'node/' . $record['nid'], array('html' => TRUE)),
          l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ' . t('Coordinate missions'), $destination, array('html' => TRUE)),
          drupal_get_title(),
        );
      }
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    }
    // Single Task
    if (arg(0) == 'task') {
      $result = db_query('SELECT n.title, n.nid FROM {node} n JOIN {og_membership} o ON o.gid=n.nid WHERE o.entity_type = \'commerce_product\' AND o.etid = :id', array(':id' => arg(1)));
      $record = $result->fetchAssoc();
      $title  = $record['title'];
      
      // Shorten the string, but respect word boundaries 
      if (strlen($title) > 32 && preg_match('/^.{1,32}\b/su', $title, $match)) {
        $title = $match[0] . "…";
      }
      $item_url = 'node/' . $record['nid'] . '/tasks';
      $item_title = t('Tasks');
      // change second link if we view missions of a task
      if (arg(3) == 'missions') {
        $item_url = 'task/' . arg(1);
        $item_title = t('Task');
      }
      $items = array(
        l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title , 'node/' . $record['nid'], array('html' => TRUE)),
        l('<span class="glyphicon glyphicon-check" aria-hidden="true"></span> ' . $item_title, $item_url, array('html' => TRUE)),
        drupal_get_title(),
      );
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    }
    // Coordination: Tasks and Missions
    if (arg(2) == 'task-coordination' || arg(2) == 'mission-coordination') {
      $node = node_load(arg(1));
      $title = $node->title;
      $items = array(
        l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title, 'node/' . arg(1), array('html' => TRUE)),
        '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ' . drupal_get_title(),
      );
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    }
    // Members
    if (arg(2) == 'members' && arg(3) == 'manage') {
      // Group
      $node = node_load(arg(1));
      $title = $node->title;
      $name = t('Administer members');
      drupal_set_title($name);
      $items = array(
        l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title, 'node/' . arg(1), array('html' => TRUE)),
        '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ' . $name,
      );
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    }
    // P2P view
    if (arg(2) == 'p2p') {
      $node = node_load(arg(1));
      $title = $node->title;
      $items = array(
        l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title, 'node/' . arg(1), array('html' => TRUE)),
        '<span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> ' . drupal_get_title(),
      );
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    }
    // Add multiple Tasks
    if (arg(2) == 'add-multiple') {
      $node = node_load(arg(1));
      $title = $node->title;
      $destination = drupal_get_destination()['destination'];
      $items = array(
        l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title, 'node/' . arg(1), array('html' => TRUE)),
        l('<span class="glyphicon glyphicon-check" aria-hidden="true"></span> ' . t('Coordinate tasks'), $destination, array('html' => TRUE)),
        t('Add multiple tasks'),
      );
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    } 
  }

  // Add p2p
  if(current_path() == 'node/add/p2p') {
    $nid = custom_misc_get_og_group_reference();
    if ($nid) {
      $node = node_load($nid);
      $title = $node->title;
      $items = array(
        l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title, 'node/' . $nid, array('html' => TRUE)),
        l('<span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> ' . t('Coordinate p2p'), 'node/' . $nid . '/p2p', array('html' => TRUE)),
        t('Add p2p'),
      );
      return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
    }
  }
  /* Group Subpage
  if (is_numeric(arg(2)) && arg(0) == 'group') {
    $node = node_load(arg(2));
    $title = $node->title;
    $items = array(
      l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $title, 'node/' . arg(2), array('html' => TRUE)),
      l('<span class="glyphicon glyphicon-cog" aria-hidden="true"></span> ' . t('Administer members'), 'node/' . arg(2) . '/members/manage', array('html' => TRUE)),
      drupal_get_title(),
    );
    return theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ol', 'attributes' => array('class' => 'breadcrumb')));
  }*/
}
