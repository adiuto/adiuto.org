<?php

/**
 * Implements hook_field_formatter_info().
 */
function custom_misc_field_formatter_info() {

  return array(
    // the key must be unique, so it's best to prefix with your module's name.
    'custom_misc_maplink' => array(
      // the label is what is displayed in the select box in the UI.
      'label' => t('Map link'),
      // field types is the important bit!! List the field types your formatter is for. look for the field type in the database table field_config
      'field types' => array('addressfield'),
      'settings' => array(
        'provider' => 'google', // default values
      ),
    ),
    'custom_misc_deadline' => array(
      // the label is what is displayed in the select box in the UI.
      'label' => t('Deadline'),
      // field types is the important bit!! List the field types your formatter is for. look for the field type in the database table field_config
      'field types' => array('datetime'),
      'settings' => array(
        'type' => 'label', // default values
      ),
    ),
    'custom_misc_priority' => array(
      // the label is what is displayed in the select box in the UI.
      'label' => t('Priority'),
      // field types is the important bit!! List the field types your formatter is for. look for the field type in the database table field_config
      'field types' => array('list_integer'),
      'settings' => array(
        'type' => 'label', // default values
      ),
    ),    
    'custom_misc_terms' => array(
      // the label is what is displayed in the select box in the UI.
      'label' => t('Categories'),
      // field types is the important bit!! List the field types your formatter is for. look for the field type in the database table field_config
      'field types' => array('taxonomy_term_reference'),
#      'settings' => array(
#        'type' => 'label', // default values
#      ),
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function custom_misc_field_widget_info() {

  return array(
    // the key must be unique, so it's best to prefix with your module's name.
    'custom_misc_taxonomy' => array(
      // the label is what is displayed in the select box in the UI.
      'label' => t('Custom select'),
      'field types' => array('taxonomy_term_reference'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
  );
}

/**
* Implements hook_field_formatter_settings_form().
*/
function custom_misc_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $form = array();
  if ($display['type'] == 'custom_misc_maplink') {
    $form['provider'] = array(
      '#type' => 'select',
      '#title' => t('Provider'),
      '#options' => array(
        'google' => t('Google'),
      ),
      '#default_value' => $settings['provider'],
    );
  }
  if ($display['type'] == 'custom_misc_deadline') {
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => array(
        'label' => t('Label'),
        'big' => t('Big'),
        'class' => t('Class'),
        'short' => t('Short'),
      ),
      '#default_value' => $settings['type'],
    );
  }
  if ($display['type'] == 'custom_misc_priority') {
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => array(
        'label' => t('Label'),
        'class' => t('Class'),
      ),
      '#default_value' => $settings['type'],
    );
  }  
  if ($display['type'] == 'custom_misc_terms') {
    $form['type'] = array(

    );
  }
  return $form;
}

/**
* Implements hook_field_widget_form().
*/
function custom_misc_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
#dsm($element);
  // Always show only one set of items regardless of form field settings!
if ($delta == 0) {
  $tags = array();
  $options = array();
  $default_value = array();
  foreach ($items as $item) {
    $tags[$item['tid']] = isset($item['taxonomy_term']) ? $item['taxonomy_term'] : taxonomy_term_load($item['tid']);
    $default_value[$item['tid']] = $item['tid'];
  }

  // Abstract over the actual field columns, to allow different field types to
  // reuse those widgets.
  $value_key = key($field['columns']);
#dsm($field);
  $multiple = $field['cardinality'] > 1 || $field['cardinality'] == FIELD_CARDINALITY_UNLIMITED;
  $required = $element['#required'];
#  $has_value = isset($items[0][$value_key]);
  $properties = array(
    'filter_xss' => TRUE,
    'strip_tags' => FALSE,
    'empty_option' => (!$required && !$multiple) ? 'options_none' : FALSE,
    'optgroups' => FALSE,
  );

  // To select the vocabulary
  // Get the field info
  $info = field_info_field($instance['field_name']);
  // The link to the vocab is stored as it's machine name
  $vocab_machine_name = $info['settings']['allowed_values'][0]['vocabulary'];
  // Load the vocab by machine name
  $vocab = taxonomy_vocabulary_machine_name_load($vocab_machine_name);
  // Finally, get the vid
  $vid = $vocab->vid;

  // Let's do it the drupal way ;)
  // Get taxonomy tree
  $terms = taxonomy_get_tree($vid, $parent = 0, $max_depth = NULL, $load_entities = TRUE);
  #dsm($terms);
  // Seperate Parents from children 
  $taxonomy = array();
  foreach ($terms as $term) {
    // we only take the first parent and ignore the rest.
    if (!isset($taxonomy[$term->parents[0]])) {
      $taxonomy[$term->parents[0]] = array();
    }
    $taxonomy[$term->parents[0]][$term->tid] = $term->name; 
  }
#dpm($taxonomy);

  $element += array(
    '#type' => 'fieldset',
    '#value_key' => $value_key,
    '#element_validate' => array('custom_misc_taxonomy_validate'),
    '#properties' => $properties,
  );


  // Not very elegant, but ok for our purpouse: Not performant and 
  // does not deal with multiple parents. We should build a tree with a
  // recursiv callback function instead. 
  // everything is now tailord for a two layer vocabulary, no need to expand it.
  foreach ($taxonomy as $parent_tid => $terms) {

    // default values for radios
    $default_radio = '';
    foreach ($terms as $tid => $name) {
      if (isset($default_value[$tid])) {
        $default_radio = $tid;
      }
    }

    // wihtout an additional enclosing fieldset #states wouldn''t differentiate
    // between the different term groups
    $element['parent_' . $parent_tid] = array(
      '#type' => 'container',
    );
    $element['parent_' . $parent_tid]['terms'] = array(
      // Radios
      '#type' => 'radios',
      '#default_value' => $default_radio,
      '#options' => $terms,
    );
    if ($parent_tid != 0) {
      $element['parent_' . $parent_tid]['#states'] = array(
        // Hide the settings when the cancel notify checkbox is disabled.
        'visible' => array(
          ':input[name="' . $instance['field_name'] . '[und][parent_0][terms]"]' => array('value' => $parent_tid),
       ),
      );
    }
  }
#dsm($element);
  return $element;
}
}

/**
* Implements hook_field_formatter_settings_summary().
*/
function custom_misc_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  if ($display['type'] == 'custom_misc_maplink') {
    $summary = t('Map provider is @provider', array('@provider' => $settings['provider']));
  }
  if ($display['type'] == 'custom_misc_deadline') {
    $summary = t('Display the deadline in the nicest way possible.');
  }
  if ($display['type'] == 'custom_misc_priority') {
    $summary = t('Display the priority in the nicest way possible.');
  }  
  if ($display['type'] == 'custom_misc_terms') {
    $summary = t('Display only one term even if there are multiple.');
  }
  return $summary;
}


/**
 *
 */
function custom_misc_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  $settings = $display['settings'];
  $output = array();
  switch ($display['type']) {
    case 'custom_misc_maplink':
      #dsm($items);
      if ($items && $items[0]) {
        $address = '';
        foreach (array_reverse($items[0]) as $item) {
          $address .= ($item) ? $item . '+' : '';
        }
        $options = array(
          'query' => array(
            'api' => 1,
            'query' => substr($address, 0 , -1), // Remove the last +
          ),
          'external' => TRUE,
        );
        $path = url('http://www.google.com/maps/search/', $options);
        $options = array('html' => TRUE, 'attributes' => array('class' => array('btn', 'btn-xs', 'btn-link')));
        $title = '<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Google';
        $output['maplink']['#markup'] = l($title, $path, $options);
      }
    break;
    case 'custom_misc_deadline':
      $user_time_zone = new DateTimeZone(drupal_get_user_timezone());
      $utc = new DateTimeZone('UTC');
      
      foreach ($items as $delta => $item) {
        if ($item['value']) {
          // TODO what about Timezones?
          // SG: I gues this way it's going to work!
          $datetime1 = new DateTime($item['value'], $utc);
          $datetime1->setTimezone($user_time_zone);
          $datetime2 = new DateTime('now', $utc);
          $datetime2->setTimezone($user_time_zone);
          $interval = $datetime1->diff($datetime2);
          $timestring = array();
          $markup = '';
          $class = 'default';
          if ($interval->y > 0) {
            $markup .= format_plural($interval->y, '1 year', '@count years');
            $class = 'default';
            if ($interval->m > 0) {
              $markup .= ' ' . format_plural($interval->m, 'and 1 month', ' and @count months');
            }
          }
          elseif ($interval->m > 0) {
            $markup .=  format_plural($interval->m, '1 month', '@count months');
            $class = $interval->m > 6 ? 'default' : 'info';
            if ($interval->d > 0) {
              $markup .= ' ' . format_plural($interval->d, 'and 1 day', ' and @count days');
            }
          }
          elseif ($interval->d > 0) {
            $markup .= format_plural($interval->d, '1 day', '@count days');
            $class = $interval->d > 3 ? 'success' : 'warning';
            if ($interval->h > 0) {
              $markup .= ' ' . format_plural($interval->h, 'and 1 hour', ' and @count hours');
            }
          }
          elseif ($interval->h > 0) {
            $markup .= format_plural($interval->h, '1 hour', '@count hours');
            $class = 'danger';
            if ($interval->i > 0) {
              $markup .= ' ' . format_plural($interval->i, 'and 1 minute', ' and @count minutes');
            }
          }
          elseif ($interval->i > 0) {
            $class = 'danger';
            $markup .= format_plural($interval->i, '1 minute', '@count minutes');
          }
          else {
            $markup .= t('Time\'s up!');
          }
          if ($interval->invert == 0) {
            $class = 'danger';
            $markup = '<span class="text text-' . $class . '"> - ' . $markup . '</span>';
          }
          #dsm($settings);
          if ($settings['type'] == 'label') {
            $output[$delta] = array(
              '#prefix' => '<span class="text text-' . $class . '"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></span> ',
              '#markup' => t('for') . ' ' . $markup,
            );
          }
          if ($settings['type'] == 'big') {
            $output[$delta] = array(
              '#prefix' => '<h4><span class="text text-' . $class . '"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></span> ',
              '#suffix' => '</h4>',
              '#markup' => $markup,
            );
          }
          if ($settings['type'] == 'class') {
            $output[$delta]['#markup'] = $class;
          }
        }
      }
    break;
    case 'custom_misc_priority':
      #dsm($items);
      
      foreach ($items as $delta => $item) {
        if ($item['value']) {
          // Fetch translation.
          if (($translate = i18n_field_type_info($field['type'], 'translate_options'))) {
            $allowed_values = $translate($field);
          }
          else {
            // Defaults to list_default behavior
            $allowed_values = list_allowed_values($field);
          }
          $class = $item['value'] > 0 ? 'warning' : 'default' ;
          $class = $item['value'] > 1 ? 'danger'  : $class ;
          
          $markup = "";
          if ($item['value'] != 0) {
            $markup = "<div class='label alert-$class priority priority-$class'>".$allowed_values[$item['value']]."</div>";
          }
          
          if ($settings['type'] == 'label') {
            $output[$delta] = array(
              '#markup' => $markup,
            );
          }
          if ($settings['type'] == 'class') {
            $output[$delta]['#markup'] = $class;
          }
        }
      }
    break;    
    case 'custom_misc_terms':
      if ($items) {
        #dsm($items);
        // we can just go through all terms because the parent term will be the first term
        foreach ($items as $term) {
          $tid = $term['tid'];
        }
        $category = taxonomy_term_load($tid);
        #dsm($category);
        $output =  array(
          'category' => array(
            '#type' => 'markup',
            '#markup' => $category->name,
          ),
        );
      }
    break;
  }
  return $output;
}

/**
 * Implements hook_field_widget_error().
 */
function custom_misc_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element, $error['message']);
}


/**
 * Form element validation handler for options element.
 */
function custom_misc_taxonomy_validate($element, &$form_state) {
  if ($element['#required'] && $element['#value'] == '_none') {
    form_error($element, t('!name field is required.', array('!name' => $element['#title'])));
  }
  // Transpose selections from field => delta to delta => field, turning
  // multiple selected options into multiple parent elements.
  
  // We have to combine all values together if we seperate them in different parts.
  // See custom_misc_field_widget_form
  if (empty($element['#value'])) {
    $element['#value'] = array();
  }
#dsm($element);
  // Parent
  if (!empty($element['parent_0']['terms']['#value'])) {
    $parent_id = $element['parent_0']['terms']['#value'];
    $element['#value'] += array($element['parent_0']['terms']['#value'] => $element['parent_0']['terms']['#value']);
    if (!empty($element['parent_' . $parent_id]['terms']['#value'])) {
      $element['#value'] += array($element['parent_' . $parent_id]['terms']['#value'] => $element['parent_' . $parent_id]['terms']['#value']);
    }
  }


  $items = _options_form_to_storage($element);
  #dsm($element);
  form_set_value($element, $items, $form_state);
}

