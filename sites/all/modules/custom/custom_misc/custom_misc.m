<?php

// Load all Block module hooks.
module_load_include('inc', 'custom_misc', 'custom_misc.block');
// Load all Field module hooks.
module_load_include('inc', 'custom_misc', 'custom_misc.field');

function custom_misc_init() {
  
}

/*
 * Implements hook_views_api().
 */
function custom_misc_views_api() {
  return array(
    'api' => '3',
  );
}

/**
 * hook_form_FORM_ID_alter
 */
function custom_misc_form_views_exposed_form_alter(&$form, &$form_state, $form_id) { 
  // Make an awesome search field converting it into a button group.
  if ($form['#id'] == "views-exposed-form-task-search-page" 
      || $form['#id'] == "views-exposed-form-task-search-page-1"
      || $form['#id'] == "views-exposed-form-task-search-block-1") {
     $form['group']['#prefix'] = '<div class="input-group col-xs-12 col-sm-10 col-lg-8 input-group-search">';
     $form['group']['#sufix']  = '</div>';
     $form['group']['query'] = $form['query'];
     $form['group']['query']['#attributes']['class'][] = "form-control";     
     $form['group']['submit'] = $form['submit'];
     $form['group']['submit']['#attributes']['class'][] = "input-group-btn-fix btn-success";  
     $form['group']['submit']['#prefix'] = '<div class="input-group-btn">';
     $form['group']['submit']['#suffix'] = '</div>';
     unset($form['query']);
     unset($form['submit']);
  }
    
}

/**
 * hook_form_FORM_ID_alter
 */
function custom_misc_form_initiative_node_form_alter(&$form, &$form_state, $form_id) {
  
  // Hide status column in inline edit form if set.
  if (isset($form['field_location']['und']['entities']['#table_fields']['status'])) {
    unset($form['field_location']['und']['entities']['#table_fields']['status']);
  }

  // Hide group reference form element on inline add form.
  $form['field_location']['und']['form']['og_group_ref']['#access'] = false;

  // Set smaller buttons.
  foreach($form['field_location']['und']['entities'] as $key => &$row) {
    if (is_int($key)) {
      $row['actions']['ief_entity_edit']['#attributes'] = array('class' => array('btn-xs'));
      $row['actions']['ief_entity_remove']['#attributes'] = array('class' => array('btn-xs'));

      // Hide group reference form element on inline edit form.
      $row['form']['og_group_ref']['#access'] = false;
    }
  }
}


/**
 * hook_form_FORM_ID_alter
 */
// set default values for product generation
function custom_misc_form_commerce_product_ui_product_form_alter(&$form, &$form_state, $form_id) {
  // Set default price to 0
  $form['commerce_price'][LANGUAGE_NONE][0]['amount']['#default_value'] = 0;
  
  // Rename stock to demand.
  $form['commerce_stock'][LANGUAGE_NONE][0]['value']['#title'] = t('Demand');
  $form['commerce_stock'][LANGUAGE_NONE][0]['value']['#type'] = 'numberfield';
  $form['commerce_stock']['#weight'] = 3;

  // Put street name into option label of locations selection.
  foreach ($form['field_location']['und']['#options'] as $nid => &$label) {
    $location = node_load($nid);
    $label   .= " (" . $location->field_address['und'][0]['thoroughfare'] . ")";
  }
  
  // Hide text format selection.
  $form['field_description']['#after_build'][] = "custom_misc_hide_text_format";

  // Hide price field.
  $form['commerce_price']['#access'] = false;

  // Hide barcode field.
  $form['field_barcode']['#access'] = false;  
  
  // Hide status field. We use an tab and a custom form to toggle status for
  // tasks.
  $form['status']['#access'] = false;  
  
  // TODO: Hide endless demand field as long as it's not implemented.
  $form['field_endless_demand']['#access'] = false;
  $form['field_endless_amount']['#access'] = false;
  
  // Rename submit field.
  $form['actions']['submit']['#value'] = t('Save');
  
  // Hide field_google_product_taxonomy.
  $form['field_google_product_taxonomy']['#access'] = false;
  
  // Overwrite some descriptions.
  $form['status']['#description'] = t("Disabled tasks cannot be added to missions and may be hidden in all public task lists. Some roles may not be authorized to activate tasks.");
  $form['status']['#weight'] = 20;
  $form['commerce_stock']['und'][0]['value']['#description'] = t("Specify the demand of this task.");
  
  // Style categories.
  #$form['field_categories']['#attributes']['class'][] = "btn-group";
  foreach($form['field_categories']['und'] as $key => $parent) {
    if(preg_match('/(parent\_\d)/usi', $key, $matches)) {
      foreach($parent['terms']['#options'] as $number => $option) {
        $form['field_categories']['und'][$matches[1]]['terms']['#options'][$number] = '<span class="icon glyphicon glyphicon-tag" aria-hidden="true"></span> ' . $option;
      }
    }
  }
  
  // Add custom validate handler
  $form['#validate'][]  = 'custom_misc_form_commerce_product_ui_validate_handler';
  #$form['#validate'][]  = 'custom_misc_deactivate_task_validate_handler';
  #$form['actions']['submit']['#submit'][]   = 'custom_misc_deactivate_task_submit_handler';
  
  $form['field_deadline']['#weight'] = 5;
  
  // Add custom submit handler.
//  $form['#submit'][] = 'custom_misc_form_commerce_product_ui_product_form_custom_submit';
   
}

/**
 * hook_form_FORM_ID_alter_submit
 */
// set default values for product generation
//function custom_misc_form_commerce_product_ui_product_form_custom_submit(&$form, &$form_state) {
//  
//}

/**
 * Custom submit handler for product form
 */
function custom_misc_deactivate_task_validate_handler($form, &$form_state) {
  if (!isset($form_state['confirm'])) {  
    #$form_state['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form
    #$form_state['rebuild'] = TRUE; // along with this
    return;
  }
  return;
}

/**
 * Custom submit handler for product form
 */
function custom_misc_deactivate_task_submit_handler($form, &$form_state) {
  if (!isset($form_state['confirm']) || !$form_state['confirm']) {
    $form_state['confirm'] = true; // this will cause the form to be rebuilt, entering the confirm part of the form
    $form_state['rebuild'] = true; // along with this
  }
  #drupal_set_message("Test", "info");
}

/**
 * Returns te drupal confirmation form
 */
function custom_misc_deactivate_task_form_confirm($form, &$form_state) {
 
  $path = "user";  
  $title = t('Are you sure you want to abord %mission?', array('%mission' => drupal_get_title()));
  $text = "";
  // Tell the submit handler to process the form
  $form_state['confirm'] = true;
  return confirm_form(array(), $title, $path, $text);
}


/**
 * Hides text formats during the form build process.
 */
function custom_misc_hide_text_format(&$form) {
  $form[LANGUAGE_NONE][0]['format']['#access'] = FALSE;
  return $form;
}


/**
 * Implementation of hook_form_FORM_ID_alter()
 */
function custom_misc_form_commerce_cart_add_to_cart_form_alter(&$form, &$form_state, $form_id) {
  $pid   = isset($form['product_id']['#value']) ? $form['product_id']['#value']: 0;
  $task  = isset($form_state['default_product']) ? $form_state['default_product'] : false;
  $stock = isset($task->commerce_stock['und'][0]['value'])? round($task->commerce_stock['und'][0]['value']): "";
  $deadline = isset($task->field_deadline['und'][0]['value']) ? $task->field_deadline['und'][0]['value'] : false;
  
  // Change label of submit button.
  if (isset($form['submit']['#disabled']) && $form['submit']['#disabled'] == FALSE) {
    $form['submit']['#value'] = t('To mission');
  }
  
  // Change label of submit button when out of stock.
  if (isset($form['submit']['#disabled']) && $form['submit']['#disabled'] == 1) {
    $form['submit']['#value'] = t('Currently no demand');
  }
  if (isset($form['submit']['#attributes']) && $form['submit']['#attributes']['disabled'] == 'disabled') {
    $form['submit']['#value'] = t('Disabled');
  }
  if (isset($form['quantity']['#title'])) {
    unset($form['quantity']['#title']);
  }
  
  // If deadline has expired deactivate button.
  if (isset($form['submit']['#disabled']) && strtotime($deadline) <= time()) {
    $form['submit']['#disabled'] == true;
    $form['submit']['#attributes']['disabled'] = 'disabled';
    $form['submit']['#value'] = t('Expired');
  }
  
  // Customize quantity form.
  $form['quantity']['#type'] = 'numberfield'; // this can only work with the elements module
  $pid   = isset($form['product_id']['#value']) ? $form['product_id']['#value']: 0;
  $task  =  entity_load_single('commerce_product', $pid);
  $stock = isset($task->commerce_stock['und'][0]['value'])? round($task->commerce_stock['und'][0]['value']): "";
  
  // Set standard label of submit button.
  if (isset($form['submit']['#disabled']) && $form['submit']['#disabled'] == FALSE) {
    $form['submit']['#value'] = t('To mission');
  }
  // Set label of submit button, if out of stock.
  if (isset($form['submit']['#disabled']) && $form['submit']['#disabled'] == 1) {
    $form['submit']['#value'] = t('Currently no demand');
  }
  // Set label of submit button, if disabled.
  if (isset($form['submit']['#attributes']) && $form['submit']['#attributes']['disabled'] == 'disabled') {
    $form['submit']['#value'] = t('Disabled');
  }
  // Unset field's title as we don't want it in the button group.
  if (isset($form['quantity']['#title'])) {
    unset($form['quantity']['#title']);
  }
  
  if (isset($form_state['build_info']['args'][0]->data['context']['view']['view_name']) 
                        && $form_state['build_info']['args'][0]->data['context']['view']['view_name'] == "task_search") {
    // Create a button group
#    $form['#prefix'] = "<div class='row'>" .  '<div class="input-group input-group-sm col-xs-8"><div class="input-group-addon">'.t("Add").'</div></div>';
#    $form['#attributes']['class'][] = 'form-inline';
#    $form['quantity']['#field_prefix'] = '<div class="input-group input-group-sm col-xs-8">';
    $form['quantity']['#attributes']['class'][] = 'hidden';
    $form['submit']['#attributes']['class'][] = 'btn-xs btn-success';
    $form['submit']['#value'] = '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ' . t('To mission');
#    $form['quantity']['#field_suffix'] = '</div>';
#    $form['#suffix'] = "</div>";
  } else {
    // Create a button group
    $form['quantity']['#suffix'] = "\n";
    $form['#attributes']['class'][] = 'form-inline';
    $form['quantity']['#field_prefix'] = '<div class="input-group"><div class="input-group-addon">'.t("Add").'</div>';
    $form['quantity']['#field_suffix'] = '<div class="input-group-addon"> '.t("of @quantity", array('@quantity' => $stock)).'</div></div>';
  }
  
  // Customize cart form.
  $form['actions']['submit']['#value'] = t('Update mission');
  $form['actions']['checkout']['#value'] = t('Finish mission planning');
}

/**
 * hook_form_FORM_ID_alter
 */
function custom_misc_form_commerce_checkout_form_checkout_alter(&$form, &$form_state, $form_id) {
  $form['commerce_fieldgroup_pane__group_custom']['#collapsible'] =1;
  $form['commerce_fieldgroup_pane__group_custom']['#collapsed'] =1;
}

/**
 * Implements hook_form_alter().
 */
function custom_misc_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'views_form_commerce_cart_form_') === 0) {
    // Only alter buttons if the cart form View shows line items.
    $view = reset($form_state['build_info']['args']);
    drupal_set_title(t("Your mission"));
    #$order = commerce_order_load($form_state['order']->order_id);
    if (!empty($view->result)) {
      // Change the Save button to say Update cart.
      $form['actions']['submit']['#value'] = t('Update mission');
      #$form['actions']['checkout']['#submit'][] = "custom_misc_checkout_line_item_views_form_submit";
    }
  }
  if (strpos($form_id, 'commerce_checkout_form_review') === 0) {
    $form['buttons']['continue']['#value'] = t('Finish planning');
    $form['buttons']['continue']['#attributes']['class'][] = "btn-success";
    $form['buttons']['back']['#value'] = t('Edit mission');
    $form['buttons']['back']['#redirect'] = "cart";
    $form['buttons']['back']['#submit'][0] = "custom_misc_form_back_submit";
  }
  if ($form_id ==  "commerce_checkout_form_checkout") {
    $form['buttons']['continue']['#value'] = t('Finish planning');
    $form['buttons']['continue']['#attributes']['class'][] = "btn-success";
    $form['buttons']['continue']['#validate'][]  = 'custom_misc_check_pending_missions';
    #dpm($form);
  }
}

/**
 * Validation callback, to check if user has pending missions.
 */
function custom_misc_check_pending_missions($form, &$form_state) {
  $uid = $form_state['order']->uid;
  
  // Use views to load pending orders.
  $view = views_get_view('commerce_user_orders');
  $view->set_display('user_pending_order');
  $view->set_arguments(array($uid));
  $view->pre_execute();
  $view->execute();
  
  // Don't allow users to check out a mission if they have too many pending.
  // missions
  if ($view->total_rows > 3) {
    form_set_error('', t('You have <a href="@pending_missions">!count pending missions.</a> You can\'t checkout a new mission before accomplishing your pending missions.!help_accomplishing', 
                         array('@pending_missions' => "/user/$uid/missions",
                               '!count' => $view->total_rows,
                               '!help_accomplishing' => custom_misc_help_link("faq-accomplish-a-mission"))) 
                         );
  } 
}

/**
 * Special submit handler for the back button to avoid processing orders.
 */
function custom_misc_form_back_submit($form, &$form_state) {
  $form_state['redirect'] = 'cart';
  $order = commerce_order_load($form_state['order']->order_id);
  $previous_page = commerce_checkout_page_load($form_state['checkout_page']['prev_page']);
  // Update the order status for the checkout step.
  $form_state['order'] = commerce_order_status_update($order, 'checkout_' . $previous_page['page_id'], 
                          FALSE, NULL, t('User returned to the mission edit page via a submit button.'));
}


/**
 * Implements hook_commerce_checkout_pane_info_alter().
 */
function custom_misc_commerce_checkout_page_info_alter(&$checkout_pages) {
#dpm($checkout_pages);
  $checkout_pages['checkout']['title'] = t("Finish planning your mission");
  $checkout_pages['checkout']['weight'] = 0;
  $checkout_pages['checkout']['help'] = t("If you continue, you pledge to make every effort to complete the tasks listed below. Please make sure to not check out any task, you are not able or willing to accomplish.");
  $checkout_pages['review']['title']   = t("Review your mission");
  $checkout_pages['review']['weight'] = 15;
  $checkout_pages['review']['help'] = t("Review your mission planning before you finish it.");
  $checkout_pages['complete']['title'] = t("Mission planning completed");
  return $checkout_pages;
}

/**
 * Allows modules to alter checkout panes defined by other modules.
 *
 * @param $checkout_panes
 *   The array of checkout pane arrays.
 *
 * @see hook_commerce_checkout_pane_info()
 */
function custom_misc_commerce_checkout_pane_info_alter(&$checkout_panes) {
  #dpm($checkout_panes);
  $checkout_panes['cart_contents']['title'] = t("Your mission's tasks");
}

/**
 * Custom validate handler for product form
 */
function custom_misc_form_commerce_product_ui_validate_handler($form, &$form_state) {
  $entity = $form['#entity'];
  
  // If the timestamp isn't set, it is probably a new entity 
  if (empty($entity->changed)) {
    #return;
  }
}

/**
 * Implements hook_node_insert().
 */
function custom_misc_node_insert($node) {
  // Locations are nodes added by an inline form when an initiaitve is created or
  // updated. As we want them to be group member of the initiaitve, we have to 
  // add it after node insertion.
  if ($node->type == "initiative" && isset($node->field_location['und'])) {
    foreach($node->field_location['und'] as $value) {
      $location = node_load($value['target_id']);
      if ($location) {
        // Add this node to the group.
        $result = og_group('node', $node->nid, array(
          'entity_type' => 'node',
          'entity' => $value['target_id'],
          'state' => OG_STATE_ACTIVE,
        ));
      }
    }
  }
  if ($node->type == "initiative"){
    /* Tweet if new initiaitve was added. */
    if (module_exists('hybridauth')) {
      if ($hybridauth = hybridauth_get_instance()) {
        try {
          if ($hybridauth->isConnectedWith('Twitter')) {
            $adapter = $hybridauth->getAdapter('Twitter');
            $path = drupal_get_path_alias('node/' . $node->nid);
#            $adapter->setUserStatus('We have joined @adiutoOrg. Please support us by taking over our tasks: https://www.adiuto.org/' . $path );
            $adapter->setUserStatus('The initiative ' . $node->title . ' has joined @adiutoOrg. Please support them by taking over their tasks: https://www.adiuto.org/' . $path );
          }
        }
        catch (Exception $e) {
          watchdog_exception('hybridauth', $e);
        }
      }
    }
  }
}

/**
 * Implements hook_node_update().
 */
function custom_misc_node_update($node) {
  // Locations are nodes added by an inline form when an initiaitve is created or
  // updated. As we want them to be group member of the initiaitve, we have to 
  // add it after node update.  
  if ($node->type == "initiative" && isset($node->field_location['und'])) {
    foreach($node->field_location['und'] as $value) {
      $location = node_load($value['target_id']);
      if ($location) {
        // Add this node to the group.
        $result = og_group('node', $node->nid, array(
          'entity_type' => 'node',
          'entity' => $value['target_id'],
          'state' => OG_STATE_ACTIVE,
        ));
      }
    }
  }
}

/**
 * Implements hook_pathauto_alias_alter().
 */
function custom_misc_pathauto_alias_alter(&$alias, &$context) {
  if ($context['type'] == "initiative" || $context['type'] == "commerce_product") {
    // Force all aliases to be saved as language neutral.
    $context['language'] = LANGUAGE_NONE;
  }
}

/**
 * Fixing the argument substitution for organic groups in views.
 */
function custom_misc_views_pre_render(&$view) {
  if($view->name == 'shop' && $view->current_display == 'page_1'){
    if ($view->result) {
      $view->build_info['substitutions']['%1'] = $view->result[0]->node_og_membership_title;
    }
    elseif (is_numeric($view->args[0])) {
      $entity = entity_load('node', array($view->args[0]), $conditions = array(), $reset = FALSE);
      $view->build_info['substitutions']['%1'] = $entity[$view->args[0]]->title;
    }
  }
//  if($view->name == 'commerce_cart_form'){
//    if ($view->result) {
//      $view->build_info['substitutions']['%1'] = "Test";
//    }
//    elseif (is_numeric($view->args[0])) {
//      $view->build_info['substitutions']['%1'] = "Test";
//    }
//  }
  if($view->name == 'task_search' && $view->current_display == 'page_1'){
    if (is_numeric($view->args[0])) {
      $entity = entity_load('node', array($view->args[0]), $conditions = array(), $reset = FALSE);
      $view->build_info['substitutions']['%1'] = $entity[$view->args[0]]->title;
    }
    // Hide search form if view has only a few rows.
    if ($view->total_rows < 10 && empty($view->exposed_input['query'])) {
      $view->exposed_widgets = NULL;
    }
  }
}

/**
 * Overriding the title of the empty cart page as it show undecoded HTML.
 */
function custom_misc_preprocess_page(&$vars, $hook) {
//  drupal_set_message(print_r($vars, true));
  $path = drupal_get_path_alias($_GET['q']);
  if ($path == 'cart') {
    drupal_set_title(t('Mission'));
    $vars['title'] = t('Mission');
  }
}


function custom_misc_views_pre_view(&$view, &$display_id, &$args) {
  if ($view->name == 'shop' && $view->current_display == 'page') {
    // $view_filters = $view->get_items('filter', $display_id); If you want to take a look at all the view filters
    $filters = $view->display_handler->get_option('filters');
    $input = $view->get_exposed_input();
  } 
  
  // Set user position as first argument and the current value of the proximity
  // filter as second.
  if ($view->name == 'task_search' && $view->current_display == 'page') {
     #$user_location = custom_geolocation_get_current_position();
     #$args[0] = $user_location['lat'] . "," .  $user_location['lon'];
  }
}


/**
 * Implements hook_module_implements_alter().
 */
function custom_misc_module_implements_alter(&$implementations, $hook) {
  // When the implementations of hook_menu_alter are called, we need our module
  // to be called after views, so let's remove it from the implementations then
  // add it to the end.
  if ($hook == 'form_alter') {
    if (isset($implementations['custom_misc'])) {
      unset($implementations['custom_misc']);
      $implementations['custom_misc'] = FALSE;
    }
  }
}

/**
 * Implements hook_views_pre_view().
 */
#function custom_misc_views_pre_view(&$view, &$display_id, &$args) {
#dpm($display_id);
#  if ($view->name == "initiative_members") {
#    $group = og_context();
#    // og_context only woks for nodes not for subpages so we just get the id from the url
#    if (!$group && is_numeric(arg(1))) {
#      $group['gid'] = arg(1);
#    }
#    $gid = $group['gid'];
#    $node = node_load($gid);

#    $members = og_get_group_members_properties($node, array(), 'members__1', 'node');
#    $uids = array();
#    
#    foreach($members as $uid) {
#      $member_roles = og_get_user_roles("node", $gid, $uid);
#      foreach($member_roles as $role) {
#        switch ($role) {
#        case "administrator";
#          $uids[$role][] = $uid;
#          break;
#        case "coordinator";
#          $uids[$role][] = $uid;
#          break;
#        case "member";
#          $uids[$role][] = $uid;
#          break;
#        }
#      }
#    }
#    $uids["member"] = array_diff($uids["member"], $uids["coordinator"], $uids["administrator"]);
#    $uids["coordinator"] = array_diff($uids["coordinator"], $uids["administrator"]);
#    dpm($uids);
#    switch ($display_id) {
#    case "page_initiative_members":
#        $view->attachment_before = "<h2>" . t("Members") . "</h2>";
#        $view->args[0] = implode(',', $uids["administrator"]);
#        dpm($view);
#        break;
#    case "attachment_initiative_coordinator":
#        $view->attachment_before = "<h2>" . t("Coordinators") . "</h2>";
#        $view->args[0] = implode(',', $uids["coordinator"]);
#        break;
#    case "attachment_initiative_administrator":
#        $view->attachment_before = "<h2>" . t("Administrators") . "</h2>";
#        $view->args[0] = implode(',', $uids["administrator"]);
#        break;
#    }
#  }
#}


/**
 * Implementation of hook_menu()
 */
function custom_misc_menu() {
  $items['node/%/members'] = array(
    'title' => t('Members'),
    'page callback' => 'custom_misc_member_views', 
    'access callback' => 'custom_misc_member_views_access_callback',
    'access arguments' => array(1), 
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 101,
  );
  $items['help/view'] = array(
    'title' => 'Help',
    'access callback' => true,
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );   
  $items['node/5/terms'] = array(
    'title' => t('Terms and Conditions'),
    'page callback' => 'custom_misc_help_tabs_callback', 
    'access callback' => true,    
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 101,
  );  
  
  return $items;
}

/**
 * Implementation of hook_menu_alter().
 */
function custom_misc_menu_alter(&$items) {
    unset($items['node']);
}

/**
 * Access callback for menu hook
 */
function custom_misc_member_views_access_callback($nid) {
  $node = node_load($nid);
  return isset($node->type) && $node->type == 'initiative' && user_access('access content');
}

/**
 * Callback for menu hook
 */
function custom_misc_help_tabs_callback($nid) {
  $node = node_load(12);
  return node_view($node);
}


/**
 * Page Callback: Shows members by role of an initiaitve.
 * TODO: This is a workaround because the view conditions for og membership
 *       seems to be broken.
 */
function custom_misc_member_views($node) {
  $group = og_context();
  // og_context only woks for nodes not for subpages so we just get the id from the url
  if (!$group && is_numeric(arg(1))) {
    $group['gid'] = arg(1);
  }
  $gid = $group['gid'];
  $node = node_load($gid);

  $members = og_get_group_members_properties($node, array(), 'members__1', 'node');
  $uids       = array('administrator' => array(), 'coordinator' => array(), 'member' => array());
  $role_names = array('administrator' => t("Administrators"), 'coordinator' => t("Coordinators"), 'member' => t("Members"));
  
  foreach($members as $uid) {
    $member_roles = og_get_user_roles("node", $gid, $uid);
    foreach($member_roles as $role) {
      switch ($role) {
      case "administrator";
        $uids[$role][] = $uid;
        break;
      case "coordinator";
        $uids[$role][] = $uid;
        break;
      case "member";
        $uids[$role][] = $uid;
        break;
      }
    }
  }
  $uids["member"] = array_diff($uids["member"], $uids["coordinator"], $uids["administrator"]);
  $uids["coordinator"] = array_diff($uids["coordinator"], $uids["administrator"]); 
  
  $title = t("Members");
  $title = drupal_get_title() ? drupal_get_title() . " – $title" : $title;
  drupal_set_title($title);

  $output = "";
  
  foreach ($uids as $role => $members) {
    $view = views_get_view('initiative_members');
    $view->set_display('attachment_members');
    $view->set_arguments(array(implode(',',$uids[$role])));
    $view->pre_execute();
    $view->execute();
    $output .= "<h3>" . $role_names[$role] . "<small>" . custom_misc_help_link("help-roles-". $role) . "</small></h3>";
    $output .= $view->render();
  }
  
  return $output;
}

/**
 * Genereates help link
 * 
 * $attributes = array(
 *    'class' => array(),
 *    'theme' => 'no_markup', //so basically to not have an outer span
 * );
 */
function custom_misc_help_link($help_id, $link_text = "", $attributes = array()) {
  
  if (!isset($help_id)) {
    return;
  }
  
  // Get language.
  global $language ;
  $lang = $language->language;
  
  // Load node with help text.
  $node = node_load(5);
  $help_text = isset($node->field_description[$lang][0]['value'])? $node->field_description[$lang][0]['value'] : $node->field_description['und'][0]['value'] ;

  // Get html-tag, containing the help ID, we need this to construct the regular 
  // expression that extracts the help text, to make it work for arbitrary tags.
  $reg = "/<([^>^<^\s]*?)\s+[^>^<]*?id=[\"']" . preg_quote($help_id, "/") . "/usi";
  preg_match($reg, $help_text, $result);
  $tag = isset($result[1])?  preg_quote($result[1]): "h2";
  
  // Extract corresponding text part .
  $reg = "/<".$tag."[^>^<]*?id=[\"']" . preg_quote($help_id, "/") . "[\"'][^>^<]*?>.*?<\/".$tag.">(.*?)(?:<".$tag."[^>^<]*?>|$)/usi";
  preg_match($reg, $help_text, $result);

  $help = "";
  
  if (isset($result[1])) {
    
    $help = strip_tags($result[1]);
    
    // Shorten the string, but respect word boundaries.
    if (preg_match('/^.{1,120}\b/su', $help, $match)) {
      $help = $match[0];
    }
    $help .= "… " . t("(Click to read more.)");
  }
  
  $options = array(
    'html' => TRUE, 
    'attributes' => array("data-original-title" => $help, "data-toggle" => "tooltip"), 
    'fragment' => $help_id,
    );
    
  // Add classes.
  if (isset($attributes['class'])) {
    $options['attributes']['class'] = $attributes['class'];
  }
  $link = l('<span class="icon glyphicon glyphicon-question-sign" aria-hidden="true"></span> ' . $link_text, 'help', $options);
 
  if (isset($attributes['theme']) && $attributes['theme'] == 'no_markup') {
    return $link;
  }
  return "<span id='$help_id'> $link</span>";
}

/**
 * Implementation of hook_entity_info_alter()
 * Overwrite human readable name of commerce orders
 */
function custom_misc_entity_info_alter(&$entity_info) {

#dsm($entity_info);
  $entity_info['commerce_order']['bundles']['commerce_order']['label'] = t('Mission');
  $entity_info['commerce_order']['label'] = t('Mission');

}


/**
 * Implements hook_form_FORM_ID_alter(): field_ui_field_edit_form
 * See: https://www.drupal.org/project/field_instance_cardinality
 * And: https://www.drupal.org/files/transform_checkboxes_into_radios-1983474-2.patch 
 */
function custom_misc_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  $field = $form['#field'];
  $instance = $form['#instance'];
  // Only suggest overriding fields whose cardinality is not 1.
  if ($field['cardinality'] != 1) {
    // We can only work with with widgets from the options module until
    // this core feature makes it in:
    if (in_array($instance['widget']['type'], array('options_select', 'options_buttons'))) {
      $form['instance']['instance_cardinality'] = array(
        '#type' => 'checkbox',
        '#title' => t("Limit number of values"),
        '#description' => t("This will limit this instance of the field to 1 value."),
        '#default_value' => isset($instance['instance_cardinality']) ? $instance['instance_cardinality'] : FALSE,
      );
    }
  }
}


/**
 * Implements hook_field_widget_form_alter().
 *
 * Too late for most widgets, but option widgets come here with all their
 * deltas and are simple FormAPI elements that we can tinker with.
 * See: https://www.drupal.org/project/field_instance_cardinality
 * And: https://www.drupal.org/files/transform_checkboxes_into_radios-1983474-2.patch
 */
 function custom_misc_field_widget_form_alter(&$element, &$form_state, $context) {
   if (!empty($context['instance']['instance_cardinality'])) {
     $element['#multiple'] = FALSE;

    if (($context['instance']['instance_cardinality'] == 1) && ($element['#type'] == 'checkboxes')) {
      $element['#type'] = 'radios';
      // Add the _none N/A type if not required
      if (!$element['#required']) {
        $element['#options'] = array('_none' => 'N/A') + $element['#options'];
      }
      // If a #default_value is set, limit to delta 0
      if (isset($element['#default_value'][0])) {
        $element['#default_value'] = $element['#default_value'][0];
      }
    }
   }
 }

/* TODO: Boost search result by priority field. 
 * This works with direct query, but does not work here:
 * http://5.45.107.69:8983/solr/adiuto.org/select?q=kinder&wt=json&indent=true&defType=dismax&bf=sum%281%2Cproduct%2820000%2Cis_field_priority%29%29
 * Seems to work now.  */
function custom_misc_search_api_solr_query_alter(&$call_args, SearchApiQueryInterface $query) {
  if ($call_args['query']) {
    // Boost factor 1000 if priority is higher then 0. recip(x,m,a,b) implementing a/(m*x+b) recip(ms(NOW,mydatefield),3.16e-11,1,1)
    #$call_args['params']['bf'][] = 'sum(1,product(1000,is_field_priority),product(5,fs_commerce_stock))';
    $call_args['params']['bf'][] = 'sum(product(1500,is_field_priority),product(0.000000036,ms(NOW,ds_field_deadline)),recip(ms(NOW,ds_created),3.16e-11,1,1))';
//    dpm($call_args);
    // Activate spellcheck. Is this wanted?
    $call_args['params']['spellcheck.collate'][] = 'true';
    
    // Boost expiering deadlines small then a day left with factor 1000. (ds_field_deadline)
    
  } else {
    // Boost if query is empty: Boost factor 2000 for priority, boost close deadline, boost new tasks.
    //$call_args['params']['bf'][] = 'sum(product(2000,is_field_priority),product(0.000000036,ms(NOW,ds_field_deadline)),recip(ms(NOW,ds_field_deadline),3.16e-11,1,1))';
    $call_args['params']['bf'][] = 'sum(product(2000,is_field_priority),product(0.000000036,ms(NOW,ds_field_deadline)),recip(ms(NOW,ds_created),3.16e-11,1,1))';
  }
}

	
