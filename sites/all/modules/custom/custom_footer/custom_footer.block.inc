<?php
/**
 * Implements hook_block_info().
 */
function custom_footer_block_info() {
  $blocks['custom_footer'] = array(
    'info' => t('Custom: Footer'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_footer_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'custom_footer':
      $block['subject'] = t('Footer');
      $block['content'] = custom_footer();
      break;  
  }
  return $block;
}

function custom_footer() {
  /****
   * Top
  */
  global $language;
  #dsm($language);
  $languages = locale_language_list('native');
  $languages_links = array();
  foreach ($languages as $lg => $lang) {
    $languages_links[$lg] = '<a href="?lang=' . $lg . '">' . $lang . '</a>';
  }
#dsm($languages_links);
  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'class' => array('btn', 'btn-default', 'btn-sm'),
      'target' => '_blank',
    ),
  );
  $top = array(
    'content' => array(
      '#prefix' => '<div class="row">',
      '#suffix' => '</div>',
      'first_para' => array(
        '#prefix' => '<div class="social col-md-9">',
        '#suffix' => '</div>',
        '#markup' => 
          l('<span class="custom-icons facebook-white"></span> Facebook', 'https://facebook.com/adiuto.org', $options) .
          l('<span class="custom-icons twitter-white"></span> Twitter', 'https://twitter.com/AdiutoOrg', $options) .
          l('<span class="custom-icons instagram-white"></span> Instagram', 'https://www.instagram.com/adiutoorg/', $options),
      ),
      'languages' => array(
        '#prefix' => '<div class="col-md-3">',
        '#suffix' => '</div>',
        'dropdown' => array(
          '#markup' => '<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuL" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">' . $language->native . ' <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></span></button>',
        ),
        'list' => array(
          '#items' => $languages_links,
          '#theme' => 'item_list',
          '#attributes' => array(
            'aria-labelledby' => 'dropdownMenuL',
            'class' => array('dropdown-menu')),
        ),
      ),
    ),
  );

  /****
   * Bottom
   */
  $attributes = array(
    'class' => array('nav', 'nav-stacked', 'nav-light'),
  );
  $titles = array();
  $nids = array(5,14,122,12,213);
  $nodes = node_load_multiple($nids); 
  foreach($nodes as $node) {
    $titles[$node->nid] = $node->title;
  }
  $bottom = array(
    'content' => array(
      '#prefix' => '<div class="row">',
      '#suffix' => '</div><div class="row text-center">© 2022 adiuto.org</div>',
      'first_para' => array(
        '#prefix' => '<div class="col-md-3 col-xs-6">',
        '#suffix' => '</div>',
        '#items' => array(
          l(t('Initiatives'), 'initiatives'),
          l(t('Tasks'), 'tasks'),
          l(t('My account'), 'user'),
        ),
        '#theme' => 'item_list',
        '#attributes' => $attributes,
      ),
      'second_para' => array(
        '#prefix' => '<div class="col-md-3 col-xs-6">',
        '#suffix' => '</div>',
        '#items' => array(
          l($titles[5], 'node/5'), // Help
          l(t('News'), 'info/news'), // Its a view
          l($titles[213], 'node/213'), // Team
        ),
        '#theme' => 'item_list',
        '#attributes' => $attributes,
      ),
      'empty_para' => array(
        '#prefix' => '<div class="col-md-3">',
        '#suffix' => '</div>',
        '#markup' => '',
      ),
      'right_para' => array(
        '#prefix' => '<div class="col-md-3 col-xs-6">',
        '#suffix' => '</div>',
        '#items' => array(
          l($titles[14], 'node/14'), // Legal Notice / Imprint
          l($titles[122], 'node/122'), // Privacy
          l($titles[12], 'node/12'), // Terms
        ),
        '#theme' => 'item_list',
        '#attributes' => $attributes,
      ),
    ),
  );
  $output = array(
    'top' => $top,
    'bottom' => $bottom,
  );

  return $output;
}