<?php
/**
 * @file
 * An example field using the Field Types API.
 */

/**
 * @defgroup custom_fields Example: Field Types API
 * @ingroup examples
 * @{
 * Examples using Field Types API.
 *
 * Providing a field requires:
 * - Defining a field:
 *   - hook_field_info()
 *   - hook_field_schema()
 *   - hook_field_validate()
 *   - hook_field_is_empty()
 *
 * - Defining a formatter for the field (the portion that outputs the field for
 *   display):
 *   - hook_field_formatter_info()
 *   - hook_field_formatter_view()
 *
 * - Defining a widget for the edit form:
 *   - hook_field_widget_info()
 *   - hook_field_widget_form()
 *
 * @see field_types
 * @see field
 */

/***************************************************************
 * Field Type API hooks
 ***************************************************************/

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function custom_fields_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'custom_fields_at' => array(
      'label' => t('Access token'),
      'description' => t('Provides an access token.'),
      'default_widget' => 'custom_fields_at_widget',
      'default_formatter' => 'custom_fields_at_formatter',
    ),
  );
}

/**
 * Implements hook_field_validate().
 *
 * @see custom_fields_field_widget_error()
 */
function custom_fields_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['at'])) {
    }
  }
}

/**
 * Implements hook_field_is_empty().
 *
 * hook_field_is_empty() is where Drupal asks us if this field is empty.
 * Return TRUE if it does not contain data, FALSE if it does. This lets
 * the form API flag an error when required fields are empty.
 */
function custom_fields_field_is_empty($item, $field) {
  return empty($item['at']);
}

/**
 * Implements hook_field_formatter_info().
 *
 * @see custom_fields_field_formatter_view()
 */
function custom_fields_field_formatter_info() {
  return array(
    'custom_fields_at_formatter' => array(
      'label' => t('Simple text-based formatter'),
      'field types' => array('custom_fields_at'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see custom_fields_field_formatter_info()
 */
function custom_fields_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {

    // This formatter simply outputs the field as text.
    case 'custom_fields_at_formatter':
      #dsm($items);
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#markup' => $item['at'],
        );
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 *
 * These widget types will eventually show up in hook_field_widget_form,
 * where we will have to flesh them out.
 *
 * @see custom_fields_field_widget_form()
 */
function custom_fields_field_widget_info() {
  return array(
    'custom_fields_at_widget' => array(
      'label' => t('Access token'),
      'field types' => array('custom_fields_at'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 *
 * hook_widget_form() is where Drupal tells us to create form elements for
 * our field's widget.
 */
function custom_fields_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['at']) ? $items[$delta]['at'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;
  #dsm($form_state);
  switch ($instance['widget']['type']) {

    case 'custom_fields_at_widget':
      $widget += array(
          '#type' => 'textfield',
          '#default_value' => $value,
          // Allow a slightly larger size that the field length to allow for some
          // configurations where all characters won't fit in input field.
          '#size' => 25,
          '#maxlength' => 25,
      );
      break;
  }

  $element['at'] = $widget;
  return $element;
}

// NOTE create a generate button doesn't work directly in the widget because it wants to save the button to the database...

/**
 * Implements hook_field_widget_error().
 *
 * hook_field_widget_error() lets us figure out what to do with errors
 * we might have generated in hook_field_validate(). Generally, we'll just
 * call form_error().
 *
 * @see custom_fields_field_validate()
 * @see form_error()
 */
function custom_fields_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'custom_fields_invalid':
      form_error($element, $error['message']);
      break;
  }
}