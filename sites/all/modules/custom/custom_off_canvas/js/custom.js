jQuery(document).ready(function($) {

  // Set top value for off canvas to make it stiky.
  var stickyNav = function(){
    
    // Check if offcanvas is present.
    if (!$(".sidebar-offcanvas").length) {
      return;
    }
    
    // Preset to value of offcanvas
    var stickyNavTop = 0;
      
    // Scroll position.
    var scrollTop = $(window).scrollTop();
    
    // Height of main content.
    var mainContentHeight = $('.main-container > div > section').height();

    // Off canvas top position.
    var offCanvasTop = $(".sidebar-offcanvas").offset().top;    
    
    // Off canvas height.
    var offCanvasHeight  = $('.sidebar-offcanvas').height();
    
    if (scrollTop > stickyNavTop) { 
      if (scrollTop + offCanvasHeight < mainContentHeight || offCanvasTop > scrollTop) { 
        //$('.sidebar-offcanvas').attr('style', 'top: ' + (scrollTop) + 'px');
        //$('.sidebar-offcanvas').animate({top: scrollTop},200);
      }
    } else {
      $('.sidebar-offcanvas').attr('style', 'top: 0px');
    }
  };
  
  // Show offcanvas on click.
  $('[data-toggle="offcanvas"]').click(function () {
    var offCanvas = $('.row-offcanvas').toggleClass('active');
    
      // change menu icon
      var toggle = document.getElementsByClassName("glyphicon-menu-hamburger");
      if (toggle.length) {
        toggle[0].className = "glyphicon glyphicon-remove";
        // document.getElementsByClassName("main-container")[0].classList.add("dark");
      }
      else {
        var toggle = document.getElementsByClassName("glyphicon-remove");
        if (toggle.length) {
          toggle[0].className = "glyphicon glyphicon-menu-hamburger";
          // document.getElementsByClassName("main-container")[0].classList.remove("dark");
        }
      }
    // Set start top position of offcanvas.
    $('.sidebar-offcanvas').attr('style', 'top: ' + 0 + 'px');
    
    // Body should not have a smaller height than offcanvas.
    var asideHeight = $('.sidebar-offcanvas').outerHeight(true) + 20;
    var sectionHeight  = $('.main-container > div > section').height();
    if (asideHeight > sectionHeight) {
      $('.main-container > div > section').animate({height: asideHeight},400);
    }
    // Make offcanvas sticky.
    stickyNav();
  });
  
  var lastScrollTop = 0;
  $(window).scroll(function(event){
     
    // Check if offcanvas is present.
    if (!$(".sidebar-offcanvas").length) {
      return;
    }
         
     var st = $(this).scrollTop();
     if (st > lastScrollTop){
       var upscroll = false;
     } else {
       var upscroll = true;
     }
     lastScrollTop = st;  
      // Off canvas top position.
      var offCanvasTop = $(".sidebar-offcanvas").offset().top;
      // Off canvas height.
      var offCanvasHeight  = $('.sidebar-offcanvas').height();
      // Scroll position.
      var scrollTop = $(window).scrollTop();
       
      if (
          // Adjust top position of offcanvas, if sum of it's top position and
          // it's height is smaller then scroll position. This makes sure, that
          // the position is only updated, when off canvas was scrolled to it's
          // end,
          offCanvasTop + offCanvasHeight < scrollTop 
          // or if top position of offcanvas is lower then scroll position
          || (offCanvasTop > scrollTop && (scrollTop > $(window).height() || upscroll == true)) 
          || scrollTop < ($(window).height() / 100 * 20) )  {
          
        stickyNav();
      }
  });
});
