<?php

/**
 * @file
 * views/ip_geoloc_plugin_argument_default_ip_geoloc.inc
 */

/**
 * Default argument plugin to inject attributes from the visitor's location.
 * Attention, this i still WIP
 */
class custom_geolocation_plugin_argument_default_custom_geolocation extends views_plugin_argument_default {

  public function option_definition() {
    $options = parent::option_definition();
    $options['type'] = array('default' => 'postal');
    return $options;
  }

  public function options_form(&$form, &$form_state) {
    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Location attribute to use'),
      '#options' => array(
        'session' => t('Point from Session'),
      ),
      '#default_value' => $this->options['type'],
    );
  }

  public function get_argument() {
    $session_location = session_cache_get('custom_location');
    if (!empty($session_location)) {
      switch ($this->options['type']) {
        case 'session':
          // 48.767,9.1827 We need a Geolcation point as a views default argument
          $location_string = $session_location['location']['latitude'] . ',' . $session_location['location']['longitude'];
          return isset($session_location['location']) ? $location_string : '';
      }
    }
    return NULL;
  }

}

