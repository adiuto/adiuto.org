<?php
/**
 * Implements hook_block_info().
 */
function custom_geolocation_block_info() {
  /*$blocks['custom_geolocation_geolocation'] = array(
    'info' => t('Custom: Device geolocation'),
    'cache' => DRUPAL_NO_CACHE,
  );*/
  $blocks['custom_geolocation_proximity'] = array(
    'info' => t('Custom: Proximity filter'),
    'cache' => DRUPAL_NO_CACHE,
  );     
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_geolocation_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    /*case 'custom_geolocation_geolocation':
      $block['subject'] = '';
      $block['content'] = custom_geolocation_geolocation();
      break;*/
    case 'custom_geolocation_proximity':
      $block['subject'] = t('Filter by proximity');
      #$block['content'] = drupal_get_form('custom_geolocation_proximity_form');
      $block['content'] = custom_geolocation_proximity();
      break;         
  }
  return $block;
}

function interpolating_polynomial($x) {
  return (((((((209 * ($x-7))/5040-37/720) * ($x-6)+1/12) * ($x-5)+1/12) * ($x-4)+1/6) * ($x-3)+1/2) * ($x-2)+2) * ($x-1);
}

/**
 * Block content callback to show form to enable user to set a custom position.
 */
function custom_geolocation_proximity($title_prefix = '') {
  $form = drupal_get_form('custom_geolocation_proximity_form');
  return $form;
}

/**
 * Custom form to set proximity filter.
 */
function custom_geolocation_proximity_form($form, &$form_state) {
 
  // Get current filter value from URL.
  $reg = '/^(.+?)(\/\d{1,2}\.?\d*[%2C,]+\d{1,2}\.?\d*)\/(\d+)?/';
  $matches = array();
  preg_match($reg, drupal_get_path_alias(), $matches);
  $current_value = isset($matches[3]) ? $matches[3] : 0;
  
  $form['proximity']['#type'] = 'radios'; 
  
  for ($i = 2; $i < 8; $i++) {
    $form['proximity']['#options'][interpolating_polynomial($i)] = "< " . interpolating_polynomial($i) . " km";
  }
  $form['proximity']['#options'][0] = t('All');
  
  $form['proximity']['#default_value'] = $current_value;
  
  // Auto submit form on change action.
  $form['proximity']['#attributes'] = array(
    'onChange' => 'document.getElementById("custom-geolocation-proximity-form").submit();',
    'class' => array('hidden'), // only show when we have the user location
  );
  
  // As we autosubmit the form on change, submit button can be hidden.
  $form['proximity']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#attributes' => array(
      'class' => array('btn-xs', 'hidden'), //get-location only for the javascript to not through error
     ),    
  );
  $form['location'] = array(
    '#suffix' => '<div id="no-permission" class="hidden">' . t('We do not have your permission<br>to access your location.') . '</div>',
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('btn', 'btn-xs', 'btn-success', 'btn-block', 'get-location'),
      'id' => 'get-location',
      'onclick' => 'getPosition()', //in geolocation.js
    ),
  );
  $form['location']['button'] = array(
    '#type' => 'markup',
    '#markup' => t('Share your location<br>so we can find nearby tasks'),
  );

  return $form;
}

/**
 * Submit handler for custom proximity filter form.
 * Redirect to origin and attaches filter arguments as path.
 */
function custom_geolocation_proximity_form_submit($form, &$form_state) {
  
  // Get current user position.
  $user_location = custom_geolocation_get_current_position();
  $arg = array();
  $arg[2] = $user_location['lat'] . "," .  $user_location['lon'];
  $arg[3] = $form_state['values']['proximity'];

  $reg = '/^(.+?)(\/\d{1,2}\.?\d*[%2C,]+\d{1,2}\.?\d*)\/(\d+)?/';
  $matches = array();
  $query = drupal_get_query_parameters();
  
  // If the reg matches, coordinates and radius are already set in path. 
  // We will ditch them in favor of new coordinates and radius.
  if (preg_match($reg, drupal_get_path_alias(), $matches)) {
    
    // Add base to path.
    $path = $matches[1];

  } else {
  
    // Add full URL to path.
    $path = drupal_get_path_alias();
    
  }

  // Redirect to origin but attach arguments to path. First is the coordinates
  // of the user, second the filter range. If range is 0 we won't
  // attach arguments to the path, as that means we it can't be filterd by
  // proximity anyway.
  if ($arg[3] && $user_location['lat'] && $user_location['lon']) {
    $path .= "/" . $arg[2] . "/" . $arg[3];
  } 
  
  // We use drupal_goto instead of $form_state['redirect'] as 
  // the latter encodes the comma to %252C instead of %2C.
  drupal_goto($path, array('query' => $query));
}

/**
 * Block content callback to show form to enable user to set a custom position. CAN BE REMOVED
 */
function custom_geolocation_geolocation($title_prefix = '') {
  $session_location = session_cache_get('custom_location');
  //var_dump($session_location['location']);
  if (empty($session_location)) {
    $markup = '<a href="#" class="get-location btn btn-primary btn-sm">' . t('Share you location<br>so we can find nearby tasks') . '</a>';
    //$markup = t('No location found');
  }
  else {
    $markup = '<span class="get-location btn btn-primary btn-sm">' . $session_location['location']['latitude'] . '/' .  $session_location['location']['longitude'] . '</span>';
  }
  $form['#prefix'] = $markup;

  /*$position = custom_geolocation_get_current_position();
  $location = !empty($position['locality'])? $position['locality']: t("Set your location.");
  $wkt = $position['wkt'];

  $title = strlen($location) > 21 ? "<span class='trimmed'>" . substr(check_plain($location),0,21).
                                    "<span class='rest'>".substr(check_plain($location),21)."</span></span>":
                                    check_plain($location);
  //$title .= " (" . t("Your location") . ")";
  $form = drupal_get_form('custom_geolocation_address_form');

  $form['location']['#title']  = '<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span> ';
  $form['location']['#title'] .= $title_prefix . $title;
  $form['location']['address']['#suffix'] = '<span class="geodata-user element-invisible">'. check_plain($wkt) .'</span>';
  $form['location']['address']['#value'] = !empty($position['address']) ?  $position['address'] : $position['locality'];
  */
  return $form;
}


/**
 * Loads the current user position form session cache and returns an array
 * with geo data
 */
function custom_geolocation_get_current_position() {

#  if(function_exists("ip_geoloc_get_current_location")) {
#    $ip_geoloc = ip_geoloc_get_current_location(NULL, NULL, true);
#  }
  $location = array(
    'locality' => t('No location found.'),
    'lat' => '',
    'lon' => '',
    'wkt' => '',
    'address' => '',
  );
  
  // The session array is filled from Javascript (geolocation.js), so we dont check anything else
  $session_location = session_cache_get('custom_location');

  // If ain't got a location from the browser, we can't proceed.
  if (empty($session_location)) {
    return FALSE;
  }

  $session_location = $session_location['location'];
  $location['address']  = !empty($session_location['route']) ? $session_location['route'] : '';
  $location['address'] .= !empty($session_location['street_number']) ? ' ' . $session_location['street_number'] . ', ': '';
  $location['address'] .= !empty($session_location['city']) ? $session_location['city'] : '';
  $location['address'] .= !empty($session_location['country']) && empty($session_location['city']) ? $session_location['country'] : '';

  // Locality is with the lat,lon the only field returned by custom geolocation.
  $location['locality'] = isset($session_location['locality']) ? $session_location['locality']: $location['address'];

  if (isset($session_location['longitude']) && isset($session_location['latitude'])) {
    $location['lat'] = $session_location['latitude'];
    $location['lon'] = $session_location['longitude'];
    $location['wkt'] = 'POINT (' . $session_location['longitude'] . ' ' . $session_location['latitude'] . ')';
  }

  return $location;
}

/**
 * Returns a Drupal form set enables user to set a custom geo position.
 */
function custom_geolocation_address_form($form_state) {
  $form['location'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['location']['address'] = array(
    '#description' => t('Enter a location'),
    '#type' => 'textfield',
    '#size' => 20,
    '#attributes' => array(
      'class' => array('input-sm'),
     ),    
  );
  $form['location']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Set location'),
    '#attributes' => array(
      'class' => array('btn-xs'),
     ),    
  );
  
  // Show reset only if position is set by user.
  $session_location = session_cache_get('custom_location');
  if (!empty($session_location)) {
    $form['location']['reset'] = array(
      '#type' => 'submit', 
      '#value' => t('Reset'),
      '#attributes' => array(
        'class' => array('btn-xs pull-right'),
       ),
      '#submit' => array('custom_geolocation_address_form_reset_submit'),
    );
  }
  return $form;
}


/**
 * Validation callback, to validate form for custom user position.
 */
function custom_geolocation_address_form_validate($form, &$form_state) {
  $address = $form_state['values']['address'];
  if (!$address) {
    form_set_error('', t('No location given.'));
  }
  elseif (!geocoder('google',$address)) {
    form_set_error('', t('Location not found.'));
  }
}

/**
 * Submit callback, handel data given by form to set custom user position.
 */
function custom_geolocation_address_form_submit($form, &$form_state) {

  $address = $form_state['values']['address'];

  $geocode = geocoder('google', $address);
  $locality = isset($geocode->data)? $geocode->data['geocoder_formatted_address']: '';

  $lat = method_exists($geocode, "y")? $geocode->y(): "";
  $lon = method_exists($geocode, "x")? $geocode->x(): "";
  $location_set = array('location' => array('latitude' => $lat, 'longitude' => $lon, 'locality' => $locality));

  session_cache_set('custom_location', $location_set);
  drupal_set_message(t('Your location was set to: @address', array('@address' => $locality)));
}

/**
 * Reset submit callback, reset user position and clear custom location from cache.
 */
function custom_geolocation_address_form_reset_submit($form, &$form_state) {
  $session_location = session_cache_set('custom_location', NULL);
  drupal_set_message(t('Location resetted.'));
}
