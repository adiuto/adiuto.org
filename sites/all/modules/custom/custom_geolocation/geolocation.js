(function ($) {
$(document).ready(function() {

	//Holt die Koordinaten des aktuellen Benutzers, wenn die Seite mit ajax geladen wird.
  Drupal.behaviors.InitReplaceGeodata = {
    attach: function(context) {
        getPosition();
    }
  }
  // time and geodata is cached to not prompt user for location on every page load
  // this is a very dirty workaround for browsers that wont save the user preference.
  var time = sessionStorage.getItem('time');
  var timePassed = Date.now() - time;

  if (timePassed < 10*60*1000) {
    var geodata = sessionStorage.getItem('geodata');
    geodata = JSON.parse(geodata); //geodata is stored as a string
    if (geodata) {
      // calculate distances for tasks
      getPoisPosition(geodata);
    }
  }
  else {
    //Holt die Koordinaten des aktuellen Benutzers, wenn die Seite gladen wird.
    getPosition();
  }
});

})(jQuery);

/*********************************************
 * Important Devicegeolocation information!
 * Firefox just works.
 * Chrome recomends an EventListener (they say its bad
 * practice to call devicegeolocation without user
 * interaction)
 * https://developers.google.com/web/fundamentals/native-hardware/user-location
 * Chromium runs out of Google API Keys around noon and can not geolocate Users: 
 * [Violation] Only request geolocation information in response to a user gesture.
 * https://bugs.chromium.org/p/chromium/issues/detail?id=753242
 * 
 * Firefox: Browsing in private mode prompts for the location every time the site is reloaded.
 */

 function getPosition() {
  var geoError = function(error) {
    console.log(error.code);
    // error.code can be:
    //   0: unknown error
    //   1: permission denied
    //   2: position unavailable (error response from location provider)
    //   3: timed out

    // Change the class and text of location filter button
    // button is defined in custom_geolocation_proximity_form
    document.querySelector('#get-location').classList.add('hidden');
    document.querySelector('#no-permission').classList.remove('hidden');
  };
  var geoSuccess = function(position) {
    // console.log(position);
    geodata = {lat: position.coords.latitude, lon: position.coords.longitude};
    console.log(geodata);
    // store geodata in session, we can check for that instead of getting the user position every time
    sessionStorage.setItem('geodata', JSON.stringify(geodata));
    // Send Position to drupal
    // URL is defined in custom_geolocation_menu() in custom_geolocation.module
    var url = '/get-location/' + geodata['lat'] + '/' + geodata['lon'];
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.onload = function() {
      if (this.status >= 200 && this.status < 400) {
        // Success!
        getPoisPosition(geodata);
      } else {
        // We reached our target server, but it returned an error
      }
    };
    request.onerror = function() {
      // There was a connection error of some sort
    };
    request.send();
  };
  var geoOptions = {
    timeout: 10 * 1000,
    maximumAge: 5 * 60 * 1000,
  };
  // We store the time of the last request to stop checking
  sessionStorage.setItem('time', Date.now());
  navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
}


/****************************************************************************
*                                      Calculate Distance
****************************************************************************/
// Holt die Koordinaten aus den POIS (WKT) und reicht sie zur Berechnung
// weiter und ersetzt sie schließlich mit dem Ergebnis der Berechnung.
var getPoisPosition = function (usersPosition) {   
  var msg  = Drupal.t('You are here!');
  var upnt = Drupal.t('Set your position.');
  if (!usersPosition) { return; };

  // if we have the user location we can show the proximity filter
  // and hide the button (in custom_geolocation_proximity_form)
  if (document.querySelector('#custom-geolocation-proximity-form')) {
    document.querySelector('#edit-proximity').classList.remove('hidden');
    document.querySelector('#get-location').classList.add('hidden');
  }

  // Geodaten aus dem Poi laden und durchlaufen
  document.querySelectorAll('.geodata:not(.processed)').forEach(function(index) {

    var point = index.textContent;

    if (point && usersPosition['lon'] > 0 && usersPosition['lat'] > 0) {

      var poisPosition = parseGeodata(point);
      
      // Abstand berechnen
      var d = calculateDistance(usersPosition['lon'], poisPosition['lon'], 
                                usersPosition['lat'], poisPosition['lat'], msg);
      // Ersetzen
        index.classList.add('processed');
        index.classList.remove('element-invisible', 'hidden');
        index.parentNode.classList.remove('element-invisible', 'hidden');
        index.textContent = d;
    } else {
      index.textContent(upnt);
    }      
  });
} 

// Erkennt Längen- und Breitengrad aus dem übergebenen Koordinaten im 
// WKT-Format.
var parseGeodata = function (point) {
  //same locations should be combined and not calculated seperatly
  //console.log(point);
  if (point != undefined) {
    var parseGeodataReg = /^(.*?)\s*\(?([0-9\.]|[\-0-9\.]*)[\s,;-]([0-9\.]|[\-0-9\.]*)\)?[\)\]\s]{0,}$/;
    var geodata = Array();
    geodata['point'] = point;
    geodata['type']  = point.replace(parseGeodataReg, '$1');
    if (geodata['type'] == "POINT") {
      geodata['lat'] = point.replace(parseGeodataReg, '$3');
      geodata['lon'] = point.replace(parseGeodataReg, '$2');
      
    } else {
      geodata['lat'] = point.replace(parseGeodataReg, '$2');
      geodata['lon'] = point.replace(parseGeodataReg, '$3');
    }
    return geodata;
  } else {
    return false;
  }
}


/**
* Calculate distance between two points on earth using the
* Haversine Formula.
*/    
  var calculateDistance = function (lon1, lon2, lat1, lat2, msg) {
  var R = 6371; // km  
  lat1 = lat1*Math.PI/180; 
  lat2 = lat2*Math.PI/180;  
  lon1 = lon1*Math.PI/180;      
  lon2 = lon2*Math.PI/180;   

  var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
              Math.cos(lat1)*Math.cos(lat2) *
              Math.cos(lon2-lon1)) * R;
  // Increas the distance to take real world situations into account.
  d = d*1.2;
    if (d >=10) {
      d = Math.round(d);
      d = d + ' km';
    } else if (d >= 0.5) {
      d = Math.round(d*10)/10;
      d = d + ' km';
    } else if (d >= 0.001) {
      d = Math.round(d*1000)/1000;
      d = d * 1000; 
      d = d + ' m';        
    } else {
      d = msg;
    }
  return d;
} 