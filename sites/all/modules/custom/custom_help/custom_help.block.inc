<?php
/**
 * Implements hook_block_info().
 */
function custom_help_block_info() {
  $blocks['custom_help_iframe'] = array(
    'info' => t('Custom: Help iframe'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_help_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'custom_help_iframe':
      $block['subject'] = t('Help block');
      $block['content'] = custom_help_iframe();
    break;  
  }
  return $block;
}

function custom_help_iframe() {

  $header = '<div class="panel-heading">
  <a id="docs-help-link" href="#" target="_blank">' . t('Open in new page') . ' <span class="glyphicon glyphicon-new-window"></span></a>
  <a href="#" id="docs-close" class="pull-right" onclick="closeShadowDOM();return false;">' . t('Close') . ' <span class="glyphicon glyphicon-remove"></span></a></div>';
  $content = '<div id="docs-help" class="panel-body"></div>';
  return '<div id="docs" class="hidden panel panel-default">' . $header . $content . '</div>';

  /*
  $url = 'https://docs.adiuto.org/de/donor-how-to';
  $iframe = '<iframe src="' . $url . '" height="700px" name="help"></iframe>';
  $footer = '<div class="text-center"><a href="' . $url . '" target="_blank">' . t('Open in new Window') . '</a></div>';
  return '<div class="sshidden">' . $iframe . $footer . '</div>';
  */
}

