 function getHelp(url) {
  console.log(url);
  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      // Success!
      var resp = this.responseText;
      // console.log(resp);
      shadowDOM(resp, url);
    } else {
      // We reached our target server, but it returned an error
    }
  };
  request.onerror = function() {
    // There was a connection error of some sort
  };
  request.send();
}

function shadowDOM(resp, url) {
  // add absolute paths to links and image urls
  resp = resp.replaceAll('href="/', 'href="https://docs.adiuto.org/').replaceAll('src="/', 'src="https://docs.adiuto.org/');
  
  //resp = resp.replaceAll(/href=\"\//ig, 'href="https://docs.adiuto.org/').replaceAll('src="/', 'src="https://docs.adiuto.org/');
  // The relevant text is between the following tags:
  var start = resp.indexOf('<div class="theme-doc');
  // category pages have a header
  if (start < 0) {
    start = resp.indexOf('<header');
  }
  var end = resp.indexOf('<footer');
  // no footer found
  if (end < 0) {
    end = resp.indexOf('</article');
  }
  resp = resp.slice(start, end);
  // Overwrite the default help link in the header
  document.querySelector('a#docs-help-link').href = url;
  // Reveal the help div, remove class hidden from #docs
  document.querySelector('#docs').classList.remove('hidden');
  // add the contents from docs.adiuto.org
  document.querySelector('#docs-help').innerHTML = resp;
  // if an id was passed in the link we scroll to that id
  var start = url.indexOf('#');
  if (start > 0) {
    id = url.slice(start+1); // cut # off the slice
    document.getElementById(id).scrollIntoView();
  }
  
  // Change all links to docs.adiuto.org to onclick Events to open inside the Help div (#docs-help)
  document.querySelector('#docs-help').querySelectorAll('a[href*="https://docs.adiuto.org/"]').forEach(
    item => item.setAttribute('onclick', 'getHelp(\'' + item.href + '\');return false;')
  );
}

function closeShadowDOM() {
  document.querySelector('#docs').classList.add('hidden');
}

jQuery(document).ready(function($) {
  let div = document.querySelector('#docs'),
  isResizing = false;

div.addEventListener('mousedown', mousedown);

function mousedown(e) {
  //get the initial mouse coordinates and the position coordinates of the element
  let prevX = e.clientX,
      prevY = e.clientY,
      rect = div.getBoundingClientRect(),
      prevLeft = rect.left,
      prevTop  = rect.top;

  if (!isResizing && !e.target.classList.contains('content')) {
      window.addEventListener('mousemove', mousemove);
      window.addEventListener('mouseup', mouseup);
  }

  function mousemove(e) {
      //get horizontal and vertical distance of the mouse move
      let newX = prevX - e.clientX, //negative to the right, positive to the left
          newY = prevY - e.clientY; //negative to the bottom, positive to the top

      //set coordinates of the element to move it to its new position
      div.style.left = prevLeft - newX + 'px';
      div.style.top = prevTop - newY + 'px';
  }

  function mouseup() {
    window.removeEventListener('mousemove', mousemove);
    window.removeEventListener('mouseup', mouseup);
  }
}


});