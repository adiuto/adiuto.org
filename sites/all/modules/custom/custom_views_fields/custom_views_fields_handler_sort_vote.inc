<?php

/**
 * @file
 * Provide a views handlers for votingapi data fields.
 */

class custom_views_fields_handler_sort_vote extends views_handler_sort {
  function option_definition() {
    $options = parent::option_definition();
    $options['coalesce'] = array('default' => FALSE);
    $options['threshold'] = array('default' => FALSE);
    $options['null_value'] = array('default' => 0);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['coalesce'] = array(
      '#type' => 'checkbox',
      '#title' => t('Treat missing votes as zeros'),
      '#default_value' => $this->options['coalesce'],
    );
    $form['threshold'] = array(
      '#type' => 'checkbox',
      '#title' => t('Treat valus with with less number of votes as zero'),
      '#default_value' => $this->options['threshold'],
    );
  }

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->ensure_my_table();
    // Add the field.

    if ($this->options['coalesce']) {
      $this->query->add_orderby(NULL, "COALESCE($this->table_alias.$this->real_field, 0)", $this->options['order'], $this->table_alias . '_' . $this->field . '_coalesced');#since we use a custom field we have to use $this->real_field to get the value!!!!
    }
    else {
      $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order']);
    }
    /**
     * https://api.drupal.org/api/views/plugins!views_plugin_query_default.inc/function/views_plugin_query_default%3A%3Aadd_field/7
     * https://api.drupal.org/api/views/plugins!views_plugin_query_default.inc/function/views_plugin_query_default%3A%3Aadd_where/7
     * https://api.drupal.org/api/views/plugins!views_plugin_query_default.inc/function/views_plugin_query_default::add_relationship/7
     * https://api.drupal.org/api/views/includes!handlers.inc/class/views_join/7
     */
    if ($this->options['threshold']) {
      #$this->query->add_where($group = 0, $field = $this->table_alias . '.' . $this->real_field, $value = '80', $operator = '<');#use table_alias . $field_alias for unique field name.
      if (isset($this->query->relationships['votingapi_cache_node_percent_vote_count'])) {
          // Only use this filter if it is exposed and selected. Otherwise it will filter the view always (If this is desired a views filter can be used)
          if (isset($this->view->exposed_raw_input) && isset($this->view->exposed_raw_input['sort_by']) && ($this->view->exposed_raw_input['sort_by'] == $this->field)) {
        /*$join = new stdClass();
        $join->table = 
        $join->extra = array(
          array(
            'value' => 'count',
          ),
        );
dsm($join);

        $this->query->add_relationship($alias = 'custom_vote_count', $join, $base = 'voting_cache', $link_point = 'node'); */


        $this->query->add_where($group = 0, $field = 'votingapi_cache_node_percent_vote_count' . '.' . $this->real_field, $value = '5', $operator = '>=');
        }
      }
      else {
        drupal_set_message(t('For the custom sort to work you need a voting api relationship that counts the number of votes (in addition to a relationship that queries the average vote).'), 'error');
      }
    }
#dsm($this->view);
  }
}
