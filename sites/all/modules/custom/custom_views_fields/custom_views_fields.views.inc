<?php

/**
 * @file
 * Views integration for the views_field_view module.
 */

/**
 * Implements hook_views_data_alter().
 */
function custom_views_fields_views_data_alter(&$data) {
#dsm($data);

  $data['votingapi_cache']['custom_value'] = array(
    'real field' => 'value',
    'title' => t('Custom rating'), 
    'help' => t('Custom display for the average vote'), 
    'field' => array(
      'handler' => 'custom_views_fields_handler_rating',
    ),
    'sort' => array(
      'handler' => 'custom_views_fields_handler_sort_vote',
    ),
  );
  $data['node']['custom_url'] = array(
    // views can only deal with real fields from the fields list
    // for a fake field a real field need to be provided.
    'real field' => 'nid',
    'title' => t('Custom link and title'),
    'help' => t('Create a link in a header and include the title.'),
    'area' => array(
      'handler' => 'custom_views_fields_handler_area_url',
    ),
  );
}
