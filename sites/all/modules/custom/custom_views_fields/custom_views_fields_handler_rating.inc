<?php
/**
 * @file
 * Views field view field handler class.
 * 
 */

class custom_views_fields_handler_rating extends views_handler_field {


  function option_definition() {
    $options = parent::option_definition();
    $options['size'] = array('default' => '');
    $options['title'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['size'] = array(
      '#type' => 'select',
      '#title' => t('Size'),
      '#options' => array(
        'big' => t('Big'),
        'small' => t('Small'),
        'micro' => t('Micro'),
      ),
      '#default_value' => $this->options['size'],
      '#required' => FALSE,
    );
    $form['title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Label'),
      '#default_value' => $this->options['title'],
      '#required' => FALSE,
    );

    $form['alter']['#access'] = FALSE;
  }


  function render($values) {
    #dsm($values);
    #dsm($this);
    $rating = $values->{$this->field_alias};
    $rating = $rating / 10;
    $class = floor($rating);
    $size = $this->options['size'];
    $rating = round($rating, 1);

    if ($size == 'micro') {
      $output =  array(
        'rating' => array(
          '#type' => 'markup',
          '#markup' => $rating,
        ),
      );
    }


    else {
      if ($rating == 0) {
        $rating = 'n/a';
      }
      $output =  array(
        'rating' => array(
          '#type' => 'markup',
          '#markup' => '<div class="scorebox scorebox-' . $size . ' scorebox-user scorebox-value-' . $class . '">' . $rating . '</div>',
        ),
      );
    }
    if ($this->options['title']) {
      $output['rating']['#markup'] .= ' <h3 class="understated">User rating</h3>';
    }
    return $output;
  }


}
