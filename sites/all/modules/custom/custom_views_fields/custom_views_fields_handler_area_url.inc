<?php
/**
 * @file
 * Views field view field handler class.
 * 
 */

class custom_views_fields_handler_area_url extends views_handler_field {


  function option_definition() {
    $options = parent::option_definition();
    $options['title'] = array('default' => '', 'translatable' => TRUE,);
    $options['url'] = array('default' => '');
    $options['arguments'] = array('default' => '');
    $options['trim'] = array('default' => '');
    $options['set'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    #$options = $this->option_definition(); Where does the variable $option come form? Maybe we need this line?
    
    $form['custom_url'] = array(
      '#type' => 'fieldset',
      '#title' => t("Title link"),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

      $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#description' => t('Title of the block. You can use [current-page:title] for the original title of the page'),
        '#default_value' => $this->options['title'],
        '#options' => $options,
        '#fieldset' => 'custom_url',
      );

    $form['trim'] = array(
      '#type' => 'checkbox',
      '#title' => t('Trim title'),
      '#default_value' => $this->options['trim'],
      '#required' => FALSE,
      '#fieldset' => 'custom_url',
    );

    $form['set'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set page title'),
      '#description' => t('Sets the page title, use with caution!'),
      '#default_value' => $this->options['set'],
      '#required' => FALSE,
      '#fieldset' => 'custom_url',
    );

      $form['url'] = array(
        '#type' => 'textfield',
        '#title' => t('Url'),
        '#description' => t('Url to link to. You can use replacment patterns.'),
        '#default_value' => $this->options['url'],
        '#options' => $options,
        '#fieldset' => 'custom_url',
      );

      $form['arguments'] = array(
        '#type' => 'textfield',
        '#title' => t('Arguments'),
        '#description' => t('Seperate all attributes with spaces. value1 value2 value3 value4 returns: ?value1=value2&value3=value4'),
        '#default_value' => $this->options['arguments'],
        '#options' => $options,
        '#fieldset' => 'custom_url',
      );



    $form['alter']['#access'] = FALSE;
  }


  function render($values) {
    if ($this->options['title']) {
      $title = t($this->options['title']);
      $title = $this->view->style_plugin->tokenize_value($title, 0);
      $title = token_replace($title); #for sidewide tokens, eg. [current-page:title]
      if ($this->options['trim'] && (strlen($title) >= 49)) {
        $title = substr($title,0,50).'…';
      }
      if ($this->options['set']) {
        drupal_set_title($title);
      }
    if ($this->options['exclude'] == 0) {
      if ($this->options['arguments']) {
        // regular expression selects "word1 words2" as pairs.
        preg_match_all("/([^ ]+) ([^ ]+)/", $this->options['arguments'], $pairs);
        // $pairs[0] holds a pair in every value, $pairs[1] holds the first value of each pair, $pairs[2] the second value.
        $query = array_combine($pairs[1], $pairs[2]);
      }
      else {
        $query = array();
      }
      // Token replacement of the url. Compare views_handler_area_text.inc
      // if no url is given create text without link
      if ($this->options['url']) {
        $url = $this->view->style_plugin->tokenize_value($this->options['url'], 0);
        $options = array(
                  'attributes' => array(
                    'class' => array('title title-link'),
                  ),
                  'query' => $query,
                  'fragment' => 'refresh',
                  'html' => TRUE,
                );

        $output = l($title, $url, $options);
      }
      else {
        $output = $title;
      }
      //maybe <h2> should be optional
      $output = '<h2>'.$output.'</h2>';
      return $output;
    }
    }
  }
}
