<?php

/**
 * Implements hook_field_info().
 *
 */
function custom_frame_field_info() {
  return array(
    'custom_frame' => array(
      'label' => t('Frame'),
      'description' => t('Display external content in a block'),
      'default_widget' => 'custom_frame_widget',
      'default_formatter' => 'custom_frame_formatter',
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function custom_frame_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['url'])) {
    }
  }
}

/**
 * Implements hook_field_is_empty().
 *
 */
function custom_frame_field_is_empty($item, $field) {
  return empty($item['url']);
}


/**
 * Implements hook_field_formatter_info().
 *
 */
function custom_frame_field_formatter_info() {
  return array(
    'custom_frame_formatter' => array(
      'label' => t('Default'),
      'field types' => array('custom_frame'),
    ),
    'custom_frame_formatter_frame' => array(
      'label' => t('Frame'),
      'field types' => array('custom_frame'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 */
function custom_frame_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'custom_frame_formatter':
      foreach ($items as $delta => $item) {
        $markup = $item['url'];
        $element[$delta] = array(
          '#markup' => $markup,
        );
      }
      break;
    case 'custom_frame_formatter_frame':
      foreach ($items as $delta => $item) {
        $url = parse_url($item['url']);
        if (isset($url['path']) && isset($url['host'])) {
          $content = custom_frame_content ($url['path'], $url['host']);
        }
        else {
           $content = t('Malformed url');
        }
        $element[$delta] = array(
          '#markup' => $content,
        );
      }
      break;
  }
  return $element;
}


/**
 * Implements hook_field_widget_info().
 *
 */
function custom_frame_field_widget_info() {
  return array(
    'custom_frame_widget' => array(
      'label' => t('Default'),
      'field types' => array('custom_frame'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 *
 */
function custom_frame_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  #$widget = $element;
  #$widget['#delta'] = $delta;
  if ($instance['widget']['type'] == 'custom_frame_widget') {
    #dsm($items);
    $element['#type'] = 'fieldset';
    $element['#title'] = 'Frame';
    $element['#description'] = '';
    $element['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => 
        t('If your initiative has a facebook page, you can enter it here and a block with the latest wall posts will be displayed on your initiative page.<br /> Use the compelte url starting with <em>https://</em> e.g. <em>https://www.facebook.com/facebook</em>.'),
      '#default_value' => isset($items[$delta]['url']) ? $items[$delta]['url'] : '',
    );
    $element['service'] = array(
      '#type' => 'select',
      '#options' => array(
        'facebook' => 'Facebook',
      ),
      '#title' => t('Service'),
      '#description' => t('Currently only facebook is supported.'),
    );
  }
  return $element;
}

/**
 * Implements hook_field_instance_settings_form().
 */
function custom_frame_field_instance_settings_form($field, $instance) {
}

function custom_frame_content ($path, $host) {
  switch ($host) {
    case 'www.facebook.com':
      $fbsdk = '<div id="fb-root"></div><script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.4&appId=1686238034943860";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));</script>';
  $fbwall = '<div class="fb-page" data-href="https://www.facebook.com' . $path . '" data-small-header="false" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>';
      return $fbsdk.$fbwall;
    default:
      return 'Not a supported service!';
  }
}
