<?php
/**
 * Implements hook_block_info().
 */
function custom_menu_block_info() {
  $blocks['custom_menu_mobile_menu'] = array(
    'info' => t('Custom: Mobile menu'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_menu_mobile_menu_tiny'] = array(
    'info' => t('Custom: Mobile menu tiny'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  $blocks['custom_menu_task_filter'] = array(
    'info' => t('Custom: Task filter'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_menu_active_facets'] = array(
    'info' => t('Custom: Active facets'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_menu_initiative'] = array(
    'info' => t('Custom: Initiative main menu'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_menu_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'custom_menu_mobile_menu':
      $block['subject'] = t('Mobile menu');
      $block['content'] = custom_menu_mobile_menu();
      break;
    case 'custom_menu_mobile_menu_tiny':
      $block['subject'] = t('Mobile menu');
      $block['content'] = custom_menu_mobile_menu_tiny();
    break;
    case 'custom_menu_task_filter':
      $block['subject'] = t('Task filter');
      $block['content'] = custom_menu_task_filter();
    break;
    case 'custom_menu_active_facets':
      $block['subject'] = t('Active facets');
      $block['content'] = custom_menu_active_facets();
    break;
    case 'custom_menu_initiative':
      $block['subject'] = t('Initiative menu');
      $block['content'] = custom_menu_initiative();
    break;    
  }
  return $block;
}

function custom_menu_mobile_menu() {
  $markup ='';
  $current_path = '/' . current_path();
  $options = array('html' => TRUE, 'attributes' => array('class' => 'text-center'));
  #dsm($current_path);

  /**********
   * Main Menu Items
   */
  $markup .= '<div class="navbar-collapse collapse navbar-left" id="navbar-collapse">';
  $items = array();
  $url = '/tasks';
  $items[] = array(
    'data' => l('<span class="glyphicon glyphicon-check" aria-hidden="true"></span> ' . t('Tasks'), $url, $options),
    'class'=> ($url == $current_path) ? array('active') : array(),
  );
  $url = '/initiatives';
  $items[] = array(
    'data' => l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . t('Initiatives') , $url, $options),
    'class'=> ($url == $current_path) ? array('active') : array(),
  );
  $url = '/node/5';
  $items[] = array(
    'data' => l('<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> ' . t('Help'), drupal_get_path_alias($url), $options),
    'class'=> ($url == $current_path) ? array('active') : array(),
  );  

  $attributes = array('class' => array('nav', 'navbar-nav', 'navbar-left'));

  $markup .= theme_item_list(array('items' => $items, 'title' => "", 'type' => 'ul', 'attributes' => $attributes));  
  $markup .= '</div>';

 
  /***
   * We could use: 
   * $orders = commerce_order_load_multiple(array(), array('uid' => $user->uid, 'status' =>'pending'));
   * but we also want to have missions for anonymous users so we have to use a view
   */
  $view = views_get_view('commerce_cart_block'); //Shopping cart block
  // Preview has to be called before results, otherwise there would be no results anyway.
  $cart = $view->preview();
  $count_items = count($view->result);

  $items = array();
  $order_badge = '';
  if (TRUE){
    $order_badge = ' <span class="badge alert-warning">' . $count_items . '</span>';
    $url = '/cart';
    $items[] = array(
      'data' => l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> <span class="user-hide">' . t('Mission') . '</span>' . $order_badge, $url, $options),
      'class'=> ($url == $current_path) ? array('active') : array(),
    );
  }

  global $user;
  $url = '/user';
  $item_classes = (arg(0) == 'user') ? array('active') : array('');
  $user_string = ($user->uid) ? format_username($user) : t('Log in');
  $items[] = array(
    'data' => l('<span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="user-hide">' . $user_string . '</span>', $url, $options),
    'class'=> $item_classes,
  ); 
  $items[] = array(
    'data' => '<a href="#" data-toggle="offcanvas" data-target=".navbar-collapse"><span class="sr-only">Toggle navigation</span><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></a>',
    'class' => array('custom-toggle'),
    );
  $attributes = array(
    'class' => array('custom-menu-user', 'nav', 'navbar-nav', 'navbar-right'));
  $markup .= theme_item_list(array('items' => $items, 'title' => "", 'type' => 'ul', 'attributes' => $attributes));



  /**********
   * Secondary Menu Items
   * The class no-menu makes them not show up in the navbar
   */
  /*
  if ($user->uid) {
    $markup .= '<div class="no-menu">';
    $items = array();
    // All your Missions
    $url = '/user/' . $user->uid . '/missions';
    $items[] = array(
      'data' => l('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ' . t('All your missions'), $url, $options),
      'class'=> ($url == $current_path) ? array('active') : array(),
    );
    // All Initiatives the user is part of
    $initiatives = views_get_view_result('user_initiatives', 'default', $user->uid);
    foreach ($initiatives as $i) {
      $url = '/node/' . $i->nid;
      $items[] = array(
        'data' => l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $i->node_title, drupal_get_path_alias($url), $options),
        'class'=> ($url == $current_path) ? array('active') : array(),
      );
    }
    $markup .= theme_item_list(array('items' => $items, 'title' => "", 'type' => 'ul', 'attributes' => $attributes));
    $markup .= '</div>';
  }
  */

  return $markup;

}

function custom_menu_mobile_menu_tiny() {
  $options = array('html' => TRUE, 'attributes' => array('class' => 'text-center'));
  $current_path = '/' . current_path();
  /**********
   * Main Menu Items
   */
  $items = array();
  $url = '/tasks';
  $items[] = array(
    'data' => l('<span class="glyphicon glyphicon-check" aria-hidden="true"></span> ' . t('Tasks'), $url, $options),
    'class'=> ($url == $current_path) ? array('active') : array(),
  );
  $url = '/initiatives';
  $items[] = array(
    'data' => l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . t('Initiatives') , $url, $options),
    'class'=> ($url == $current_path) ? array('active') : array(),
  );
  $url = '/node/5';
  $items[] = array(
    'data' => l('<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> ' . t('Help'), drupal_get_path_alias($url), $options),
    'class'=> ($url == $current_path) ? array('active') : array(),
  );

  $attributes = array(
    'class' => array('nav', 'nav-pills','text-center'),
    'id' => array('custom-menu-tiny'),
  );
  $markup = theme_item_list(array('items' => $items, 'title' => "", 'type' => 'ul', 'attributes' => $attributes));  
  return $markup;

}


  /***
   * Array of blocks to render as filters
   * We use 'module_name' => 'block_delta' for the array
   * to find the block delta one can look at the block id:
   * The id of the block is a combination of module_name . _ . block_delta.
   * Or from the block edit page ist part of the url.
   */
function custom_menu_task_filter() {
  $blocks = array(
    array(
      'module_name' => 'custom_geolocation',
      'block_delta' => 'custom_geolocation_proximity',
      'subject' => '<span class="small glyphicon glyphicon-map-marker" aria-hidden="true"></span> ' . t('Proximity')),
    array(
      'module_name' => 'facetapi',
      'block_delta' => '1mauKznbdQY9qrk9hL5w8Mi8cqDFlNAE',
      'subject' => '<span class="small glyphicon glyphicon-tags" aria-hidden="true"></span> ' . t('Categories')), //Categories
    array(
      'module_name' => 'facetapi',
      'block_delta' => 'eUFMell5yxqjPfTpDCpR1AgG0yiPS2m2',
      'subject' => '<span class="small glyphicon glyphicon-map-marker" aria-hidden="true"></span> ' . t('City')), //Locality
    array(
      'module_name' => 'facetapi',
      'block_delta' => '1p9CWnSCY70Zl67eDUPxmPQbKYtsayOF',
      'subject' => '<span class="small glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . t('Initiatives')), //Initiative
    array(
      'module_name' => 'facetapi',
      'block_delta' => 'nwDa7rZ0J4DhCOFstwVdlDBf0wTMbuXW',
      'subject' => '<span class="small glyphicon glyphicon-home" aria-hidden="true"></span> ' . t('Locations')), //Location
  );
  // Create the toggle button
  $markup = '<button id="custom-filter" onclick="openFilters()" type="button" class="btn btn-success"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span> ' . t('Filter') . '</button>';
  $markup .= '<div id="custom-menu-filter-block" class="hidden block-facetapi">'; //used in custom_menu.js to toggle
  // Render the blocks
  foreach ($blocks as $b) {
    $block = module_invoke($b['module_name'], 'block_view', $b['block_delta']);
    //dsm($block);
    $markup .= '<h4>' . $b['subject'] . '</h4>'; //block title
    $markup .= render($block['content']);
  }
  $markup .= '</div>';
  return $markup;
}

/***
 * Display location and initiative on top of Tasks list
 */
function custom_menu_active_facets() {
  $markup = '';
  $searchers = facetapi_get_active_searchers();
  foreach($searchers as $searcher) {
    // Build array of current active facets, keyed by parent if the facet is hierarchical.
    $filter_values = array();
    $adapter = facetapi_adapter_load($searcher);
    foreach ($adapter->getAllActiveItems() as $item) {
      $facet_name = $item['field alias'];
      // Only show initiatives and drop off locations
      if ($facet_name == 'og_group_ref' || $facet_name == 'field_location') {
        $facet_value = $item['value'];
        $facet = facetapi_facet_load($facet_name, $searcher);
        #dsm($facet_name . ' ' . $facet_value);
        // Get human value by applying mapping, if one is defined for this facet.
        if (isset($facet['map callback']) && is_callable($facet['map callback'])) {
          $map = call_user_func($facet['map callback'], array($facet_value), $facet['map options']);
          if (isset($map[$facet_value])) {
            $filter_values[$facet_name][] = $map[$facet_value];
            dsm($map[$facet_value]);
          }
        }
      }
    }
  }
  // Do not show initiatives on the initiative task page, that would be redundent
  if(arg(0) != 'node') {
    // Markup for the initiative filter
    if ($filter_values['og_group_ref']) {
      $markup .= '<div>';
      foreach($filter_values['og_group_ref'] as $initiative) {
        $markup .= '<span><span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $initiative . '</span>';
      }
      $markup .= '</div>';
    }
    // When no initiative is selected display 'all inititives'
    else {
      $markup .= '<div><span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . t('All initiatives') . '</div>';
    }
  }
  // Markup for the location filter
  if ($filter_values['field_location']) {
    $markup .= '<div>';
    foreach($filter_values['field_location'] as $location) {
      $markup .= '<span><span class="glyphicon glyphicon-home" aria-hidden="true"></span> ' . $location . '</span>';
    }
    $markup .= '</div>';
  }
  // When no location is selected display 'all locations'
  else {
    $markup .= '<div><span class="glyphicon glyphicon-home" aria-hidden="true"></span> ' . t('All locations') . '</div>';
  }
  $markup .= $human_value;
  return $markup;
}

/***
 * Initiative menu
 */
function custom_menu_initiative() {
  $nid = arg(1);
  $options = array('html' => TRUE);

  $items = array();
  // About us
  $url = 'node/' . $nid;
  $items[] = l('<span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . t('About us'), $url, $options);
  // Tasks
  $url = 'node/' . $nid . '/tasks';
  $items[] = l('<span class="glyphicon glyphicon-check" aria-hidden="true"></span> ' . t('Tasks'), $url, $options);

  if (og_is_member('node', $nid)) {
    // Members
    $url = 'node/' . $nid . '/members';
    $items[] = l('<span class="glyphicon glyphicon-check" aria-hidden="user"></span> ' . t('Members'), $url, $options);
  }

  /*
  $members_items = array();
  if (og_is_member('node', $nid)) {
    // Members
    $url = 'node/' . $nid . '/members';
    $members_items[] = l('<span class="glyphicon glyphicon-check" aria-hidden="user"></span> ' . t('Members'), $url, $options);
    // Leave
    $url = 'group/node/' . $nid . '/unsubscribe';
    $members_items[] = l('<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> ' . t('Leave the initiative'), $url, $options);

    if (og_user_access('node', $nid, "administer group")) {
      // Edit
      $url = 'node/' . $nid . '/edit';
      $members_items[] = l('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> ' . t('Edit') , $url, $options);
      // Translate
      $url = 'node/' . $nid . '/translate';
      $members_items[] = l('<span class="glyphicon glyphicon-globe" aria-hidden="true"></span> ' . t('Translate') , $url, $options);
    }
    $dropdown = '<a class="dropdown-toggle btn-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-option-vertical"></span> ' . t('more')  . '</a>';
    // Sub-menu
    $attributes = array('class' => array('dropdown-menu'));
    $items[] = $dropdown . adiuto_item_list(array('items' => $members_items, 'title' => "", 'type' => 'ul', 'attributes' => $attributes));  
  }
  */

  $attributes = array('class' => array('nav', 'page-navigation', 'nav-light'));
  $menu = theme_item_list(array('items' => $items, 'title' => '', 'type' => 'ul', 'attributes' => $attributes));

  return '<div class="row">' . $menu . '</div>';
}