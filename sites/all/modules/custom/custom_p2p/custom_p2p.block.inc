<?php

/**
 * Implements hook_block_info().
 */
function custom_p2p_block_info() {
  $blocks['custom_p2p_confirmation'] = array(
    'info' => t('Custom: P2P confirmation form'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_p2p_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'custom_p2p_confirmation':
      //$block['subject'] = t('Search');
      $block['content'] = custom_p2p_confirmation_block();
      break;
  }
  return $block;
}

/**
 * Block content callback to show a confirmation button on ready to be fetched tasks
 */
function custom_p2p_confirmation_block() {
  $form = drupal_get_form('custom_p2p_confirmation_form');
  
  $nid = arg(3);
  $wrapper = entity_metadata_wrapper('node', $nid);
  $pid = $wrapper->field_task_reference->getIdentifier();
  
  drupal_set_title(t("P2P @P2Pid handover", array('@P2Pid' => "T" . $pid ."P2P" . arg(3))));
  return $form;
}

/**
 * Block content callback to create the custom button form
 */
function custom_p2p_confirmation_form($form, &$form_state) {
  
  $form['confirm_p2p'] = array();
  
  $form['confirm_p2p']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Complete P2P'),
    '#submit' => array('custom_p2p_confirmation_form_submit'),
    '#validate' => array('custom_p2p_confirmation_form_validate'),
    '#attributes' => array('class' => array('btn-success btn btn-primary form-submit btn-lg')),
    '#disabled' => false,
  ); 
  
  // Save args to form state to be used in submit handler.
  $form_state['storage'] = array (
    '#nid'  => arg(3),
    '#gid'  => arg(1),
  );
  
  return $form;
}


/**
 * Submit handler of the custom_p2p_confirmation_form
 *
 */
function custom_p2p_confirmation_form_submit($form, &$form_state) {
  
  $nid = $form_state['storage']['#nid'];
  
  // Load node into wrapper, change status and save.
  try {
    $wrapper = entity_metadata_wrapper('node', $nid);
    $wrapper->field_status = 'fetched';
    $wrapper->save();
  } 
  catch (EntityMetadataWrapperException $exc) {
    watchdog(
      'custom_p2p',
      'EntityMetadataWrapper exception in %function() <pre>@trace</pre>',
      array('%function' => __FUNCTION__, '@trace' => $exc->getTraceAsString()),
      WATCHDOG_ERROR
    );
  }
  
  drupal_set_message(t("The status of @P2Pid has been set to ›fetched‹.", array('@P2Pid' => $nid)));
}

/**
 * Validate handler of the custom_p2p_confirmation_form
 *
 */
function custom_p2p_confirmation_form_validate($form, &$form_state) {
  
  $nid = $form_state['storage']['#nid'];
  $gid = $form_state['storage']['#gid'];
  
  // Load user and roles.
  global $user;
  $role = user_role_load_by_name('administrator');
  
  // Test whether node exists.
  if(!$node = node_load($nid)) {
    form_set_error('form', t("P2P node @nid does not exist.", array('@nid' => $nid)));
    return;
  }
  
  // Test whether user has permisson to edit node.
  if (!user_has_role($role->rid, $user) && !og_is_member('node', $gid)) {
    form_set_error('form', t("You @uid are not a member of group @gid.", array('@uid' => $user->uid, '@gid' => $gid)));
    return;
  }
  
  // Test whether node is in group.
  if (!og_is_member('node', $gid, 'node', $node)) {
    form_set_error('form', t("P2P node @nid is not part of group @gid", array('@nid' => $nid, '@gid' => $gid)));
    return;
  }
  
  // Test if status is deliverd.
  $wrapper = entity_metadata_wrapper('node', $nid);
  if ($wrapper->field_status->value() != 'delivered') {
    form_set_error('form', t("P2P node @nid has the status ›@status‹ but only P2P nodes with status ›delivered‹ can be confirmed.", 
                             array('@nid' => $nid, '@status' => $wrapper->field_status->value())));
  }
  
  return;
}
