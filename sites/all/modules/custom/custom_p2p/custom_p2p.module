<?php
// Load all Block module hooks.
module_load_include('inc', 'custom_p2p', 'custom_p2p.block');

/**
 * Implements hook_views_api().
 */
function custom_p2p_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_menu().
 */
function custom_p2p_menu() {
  $items = array();
  $items['node/%node/p2p/%node/spun'] = array(
    'title' => 'P2P pick up notification',
    'page callback' => 'custom_p2p_pick_up_notification',
    'page arguments' => array(1, 3),
    'access arguments' => array(1),
    'access callback' => "custom_p2p_spun_access_callback",
  );  
  return $items;
}

/**
 * Access callback to check if user have the permission to send pickup
 * notification.
 **/
function custom_p2p_spun_access_callback($group) {
  
  global $user;
  $gid = is_numeric($group) ? $group: $group->nid;
  $role = user_role_load_by_name('administrator');
  
  if (og_user_access('node', $gid, "p2p send pickup notification")
      || user_has_role($role->rid)
     ) {
      
    return true;
  }
  return false;  
}


/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function custom_p2p_rules_event_info() {
  return array(
    'p2p_pick_up_notification' => array(
      'label' => t('A link to send pick up notification has been clicked.'),
      'module' => 'custom_p2p',
      'group' => 'Custom misc',
      'variables' => array(
        'initiative' => array('type' => 'node', 'label' => t('The initiative (og) node managing the p2p reference.')),
        'p2p_node'   => array('type' => 'node', 'label' => t('A node representing the p2p reference.')),
      ),
    ),
  );
}

/**
 * Custom form to ask for confirmation before sending the P2P pick
 * up notification.
 **/
function custom_p2p_pick_up_notify_confirmation_form($form, &$form_state) {
  
  // We save the page arguments representing initiaitve and p2p node into the
  // form to load them in form's submit handler.
  $form_state['storage'] = array (
    '#group_id' => arg(1),
    '#node_id'  => arg(3),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#disabled' => false,
  );
  $form['#validate'][] = 'custom_p2p_pick_up_notify_confirmation_form_validate';
  $form_state['redirect'] = drupal_get_destination();
  
  return $form;
}

/**
 * Submit handler of the custom_p2p_pick_up_notify_confirmation_form
 **/
function custom_p2p_pick_up_notify_confirmation_form_submit($form, &$form_state) {
  
  $group = node_load($form_state['storage']['#group_id']);
  $node  = node_load($form_state['storage']['#node_id']);
  rules_invoke_event('p2p_pick_up_notification', $group, $node);
  drupal_set_message('P2P pickup notification has been send.', 'status');
}

/**
 * Validation handler of the custom_p2p_pick_up_notify_confirmation_form
 */
function custom_p2p_pick_up_notify_confirmation_form_validate($form, &$form_state) {
  
  $gid = $form_state['storage']['#group_id'];
  
  if (!custom_p2p_spun_access_callback($gid) ) {
    form_set_error('form', 'You do not have permission to send P2P notifications.');
  }
}

/**
 * Menu callback
 **/
function custom_p2p_pick_up_notification($group, $node) {
  
  // Check if node type is P2P
  if ($node->type != "p2p") {
    return "Wrong node type.";
  }
  // Load the form.
  $form = drupal_get_form("custom_p2p_pick_up_notify_confirmation_form");
  
  // Load node into wrapper.
  $node_wrapped = entity_metadata_wrapper('node', $node);
  
  $p2p_mail   = $node_wrapped->field_email->value();
  $p2p_status = $node_wrapped->field_status->value();
  $p2p_notified = $node_wrapped->field_p2p_notified->value();
  $task       = $node_wrapped->field_task_reference->value();
  
  $markup = "";
  
  if ($p2p_status != "delivered") {
    $markup = '<div class="bs-callout bs-callout-danger"> <h4>' 
          . t("You cannot send a pick up notification to %mail!", array('%mail' => $p2p_mail))
          . '</h4>'
          . t("To send a pick up notification the status of the P2P refernce no. %node_nid has to be set to \"deliverd\" but the current status is \"%status\"", 
            array('%node_nid' => $node->nid, '%status' => $p2p_status))
          . "<br>"
          . t("If a mission containig a tasks referenced by this P2P reference has been completed the P2P reference should be set programmatically to \"delivered\".", array('@task' => $task->title))
          . '<p></p> </div>';
          
    // TODO: Disable does not work, but way?      
    #$form['submit']['#disabled'] = true;
    $form['submit'] = "";
    
  } elseif ($p2p_notified) {
    $markup = '<div class="bs-callout bs-callout-danger"> <h4>' 
          . t("A Pick up notification to %mail! has already been send.", array('%mail' => $p2p_mail))
          . '</h4>'
          .' </div>';
    // TODO: Disable does not work, but way?      
    #$form['submit']['#disabled'] = true;
    $form['submit'] = "";
    
  } else {
    $markup = '<p></p><div class="bs-callout bs-callout-warning"> <h4>' 
            . t("Do you want to send a pick up notification to %mail?", array('%mail' => $p2p_mail))
            . '</h4><p>'
            . t("This will notifiy the person wo requested <strong>@task</strong> that the item has been deliverd and can be picked up.", array('@task' => $task->title))
            . '<p></p> </div>';
    
  }
  $form['#prefix'] = $markup;

  return $form;
}


/**
 * Provide information about our custom placeholder/token.
 *
 * @see http://api.drupal.org/api/drupal/modules--system--system.api.php/function/hook_token_info/7
 * @return array
 *   An associative array of available tokens and token types.
 */
function custom_p2p_token_info() {
  $info['tokens']['node']['custom-barcode-url'] = array(
      'name' => t('Custom barcode url'),
      'description' => t('Display a link to the barcode image.'),
  );
  $info['tokens']['node']['custom-barcode-embed'] = array(
      'name' => t('Custom barcode embed'),
      'description' => t('Display a link to the barcode image.'),
  );
  return $info;
}

/**
* Provide replacement values for placeholder tokens.
*
* @param string $type
*   The machine-readable name of the type (group) of token being replaced, such
*   as 'node', 'user', or another type defined by a hook_token_info()
*   implementation.
* @param array $tokens
*   An array of tokens to be replaced. The keys are the machine-readable token
*   names, and the values are the raw [type:token] strings that appeared in the
*   original text.
* @param array $data (optional)
*   An associative array of data objects to be used when generating replacement
*   values, as supplied in the $data parameter to token_replace().
* @param array $options (optional)
*   An associative array of options for token replacement; see token_replace()
*   for possible values.
* @return array
*   An associative array of replacement values, keyed by the raw [type:token]
*   strings from the original text.
*
* For mail attachments we need the url of the QRCode so we generate a Token
* to be used the the mail that is send out by Rules
*/
function custom_p2p_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);
  if ($type == 'node' && !empty($data['node'])) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'custom-barcode-url':
          $node = $data['node'];
          $barcode_value = $node->field_barcode[LANGUAGE_NONE][0]['value'];
          watchdog('custom_p2p', $barcode_value);
          
          // We dont need the base url for attachments.
          $barcode_value = 'sites/default/files/barcodes/' . md5($barcode_value) . 'QRCODE.png'; // reference barcode.plugin.inc:71
          $replacements[$original] = $barcode_value;
          break;
        case 'custom-barcode-embed':
          $node = $data['node'];
          $barcode_value = $node->field_barcode[LANGUAGE_NONE][0]['value'];
          watchdog('custom_p2p', $barcode_value);
          $barcode_value = file_create_url('public://barcodes/' . md5($barcode_value) . 'QRCODE.png'); // reference barcode.plugin.inc:71
          $replacements[$original] = $barcode_value;
          break;
      }
    }
  }
  return $replacements;
}

/**
 * Implements hook_entity_update()
 */
function custom_p2p_entity_update($entity, $type) {
  
  if ($type != "commerce_order") {
    return;
  }
  
  // This routine does the following steps:
  // When a mission is completed:
  //   For each task of the mission:
  //     If the task is P2P related:
  //       For the number of task:
  //         Set the oldest P2P in the requested state to delivered.
  
  // List with altered P2P nodes later to be filled.
  $result_msg = "";
  
  // Load mission (commerce order) into wrapper.
  $commerce_order     = entity_metadata_wrapper($type, $entity);
  $commerce_order_org = entity_metadata_wrapper($type, $entity->original);

  // Check past and current mission status. We only want to continue if the 
  // mission status is complete and was something else before.
  if ($commerce_order->status->value() != "completed"
      || $commerce_order_org->status->value() == "completed"
      ) {
    return;
  }

  // Iterate through line items.
  foreach ($commerce_order->commerce_line_items as $line_item) {
    
    $quantity = $line_item->quantity->value();
    $pid = $line_item->commerce_product->product_id->value();
    
    // Get all referencing P2P nodes sorted by creation date.
    $query = new EntityFieldQuery();
    $result = $query
    ->entityCondition('entity_type', 'node')
    ->fieldCondition('field_task_reference', 'target_id', $pid)
    ->fieldCondition('field_status', 'value', 'requested')
    ->propertyOrderBy('created', 'ASC')
    ->range(0,$quantity)
    ->execute();
    
    // Iterate through all P2P nodes and set them to delivered.
    foreach ($result['node'] as $nid => $row) {
      $p2p = entity_metadata_wrapper('node', $nid);
      $p2p->field_status = 'delivered';
      
      // We save a reference to the mission by which the
      // P2P node was set to deliverd to the P2P node.
      if (isset($p2p->field_mission_reference)) {
        $p2p->field_mission_reference->set($commerce_order->getIdentifier());
      }
      
      $p2p->save();
      $result_msg .= '<li><a href="' . url("node/$nid/edit") . '">' . $nid ."</a></li>";
    }
  }
  
  // Send a drupal message with all node IDs of alterd P2P nodes.
  // TODO: This is ugly.
  if ($result_msg) {
    $msg  = t("The following P2P nodes have been set to »deliverd« by confirming mission no @mission:", 
              array('@mission' => $commerce_order->order_id->value()));
    $msg .= "<ul>$result_msg</ul>";
    drupal_set_message($msg);
  }
}

/**
 * Implements hook_entity_update()
 */
function custom_p2p_node_update($node) {
  if ($node->type == 'p2p') {
    $p2p = entity_metadata_wrapper('node', $node);
    $tid = $p2p->field_task_reference->getIdentifier();  
    custom_synchronize_task_demand_with_p2p_references($node);
    custom_p2p_set_p2p_note_to_task_description($tid, $node);
  }
}

/**
 * Implements hook_node_insert()
 */
function custom_p2p_node_insert($node) {
  if ($node->type == 'p2p') {
    $p2p = entity_metadata_wrapper('node', $node);
    $tid = $p2p->field_task_reference->getIdentifier();
    custom_synchronize_task_demand_with_p2p_references($node, 1);
    custom_p2p_set_p2p_note_to_task_description($tid, $node);
    
  }
}

/**
 * Implements hook_node_delete()
 */
function custom_p2p_node_delete($node) {
  if ($node->type == 'p2p') {
    $p2p = entity_metadata_wrapper('node', $node);
    $tid = $p2p->field_task_reference->getIdentifier();
    custom_synchronize_task_demand_with_p2p_references($node, -1);
    
    // If node has an active P2P state it might be the last one and so
    // we call for reset or deletion of the P2P note.
    if (in_array($p2p->field_status->value(), array('requested', 'delivered'))) {
      custom_p2p_set_p2p_note_to_task_description($tid, $node);
    }
  }
}

/**
 * Implements hook_node_presave()
 */
function custom_p2p_node_presave($node) {
  if ($node->type =='p2p' && $node->nid) {

    // Get group id.
    $p2p = entity_metadata_wrapper('node', $node);
    $gid = $p2p->og_group_ref->getIdentifier();
        
    // Programmatically fill barcode field with URL for P2P nodes.
    $node->field_barcode[LANGUAGE_NONE][0]['value'] = url( 'node/' . $gid . '/p2p/' . $node->nid . '/fetch' , array('absolute' => true));
  }
}

/**
 * Synchronizes the demand (commerce stock) of a task referenced by p2p nodes 
 * with the number of p2p nodes referencing it. Whenever a task is referenced 
 * by p2p nodes, the task's demand has to match the number of p2p nodes with 
 * status "requested" referencing it.
 * This function implements a synchronization proccess and shall be called on 
 * any action which possibly changes the number of p2p nodes referencing a 
 * task; such as node update, insert or delete.
 *
 * @param entity $node
 *   The p2p node that was changed (inserted, updated or deleted).
 *
 * @param int
 *   A summand that has to be added to the new demand calculated by the sum
 *   of all referencing p2p nodes. As Drupal does not have after save or after
 *   delete hooks the function is triggered before we find the new node added
 *   or the current one deleted from the database. So, in case of node insert
 *   we add 1 in case delete we subtract 1.
 */
function custom_synchronize_task_demand_with_p2p_references($node, $summand = 0) {
  
  // If something goes wrong with the wrapper.
  try {
  
    // Load p2p node into wrapper.
    $p2p = entity_metadata_wrapper('node', $node);
    
    // Load task (commerce produt) into wrapper.
    $task  = entity_metadata_wrapper('commerce_product', $p2p->field_task_reference->getIdentifier());

    // Get all referencing P2P nodes sorted by creation date.
    $query = new EntityFieldQuery();
    $result = $query
    ->entityCondition('entity_type', "node")
    ->entityCondition('bundle', 'p2p')
    ->fieldCondition('field_task_reference', 'target_id', $task->getIdentifier())
    ->fieldCondition('field_status', 'value', 'requested')
    ->propertyOrderBy('created', 'ASC')
    ->count()
    ->execute();
    
    // If a task is checked out its demand gets down. For the time between 
    // checking a task out in a mission and completing the mission there is 
    // a discrepancy between the total number of referencing p2p nodes and the 
    // actual demand of a task that is correct, and there for should not be 
    // overwritten by the process adjusting the total number of references.
    // For this very reason we get all line items of the task from all missions 
    // with status 'pending'and sum their quantity.
    // Get all line items of the task from all missions with status 'pending'.
    $line_item_sum = round(db_query(
      "SELECT SUM(commerce_line_item.quantity) AS commerce_line_item_quantity
        FROM `commerce_line_item` commerce_line_item
        LEFT JOIN `field_data_commerce_product` field_data_commerce_product 
          ON commerce_line_item.line_item_id = field_data_commerce_product.entity_id 
          AND (field_data_commerce_product.entity_type = 'commerce_line_item' 
          AND field_data_commerce_product.deleted = '0')
        LEFT JOIN `commerce_product` commerce_product_field_data_commerce_product 
          ON field_data_commerce_product.commerce_product_product_id = commerce_product_field_data_commerce_product.product_id
        LEFT JOIN `commerce_order` commerce_order_commerce_line_item 
          ON commerce_line_item.order_id = commerce_order_commerce_line_item.order_id
        WHERE (( (commerce_product_field_data_commerce_product.product_id = :product_id ) )
        AND(( (commerce_order_commerce_line_item.status IN  ('pending')) )))",
        array(':product_id' => $task->getIdentifier())
    )->fetchColumn());

    // New demand is the sum of all referencing p2p nodes minus the sum of 
    // all inline items derived from the current task which are in pending 
    // missions.
    $new_demand = $result - $line_item_sum + $summand;
    $demand = round($task->commerce_stock->value());
    
    // Set task (commerce product) demand to the number of referencing task.
    if ($demand != $new_demand) {
      $task->commerce_stock = $new_demand;
      $task->save();
    }
    
    // If a task is referenced by a p2p node but has the status 0 (deactiated)
    // we set it to 1 (active).
    if (!$task->status->value() && $new_demand) {
      $task->status = 1;
      $task->save();
    }
    
  } catch (EntityMetadataWrapperException $exc) {
    watchdog(
      'custom_p2p',
      'EntityMetadataWrapper exception in %function() <pre>@trace</pre>',
      array('%function' => __FUNCTION__, '@trace' => $exc->getTraceAsString()),
      WATCHDOG_ERROR
    );
  }
}

/**
 * When ever a P2P node is inserted a notice is added to the referenced task,
 * prompting the user to stick the task number to the item.
 * If all P2P nodes referencing a task are deleted or set to in inactive status
 * the notice is removed. 
 *
 * @param int $tid
 *   The ID of a task (commerce product).
 *
 * @param int $node
 *   The P2P node from which the action is triggered.
 * 
 */
function custom_p2p_set_p2p_note_to_task_description($tid, $node) {
  
  $w_task = entity_metadata_wrapper('commerce_product', $tid);
 
  $task = $w_task->value();
  $description = $task->field_description['und'][0]['value'];
  
  // Regular expression to match the note.
  $reg = '/<p[^>]*id="p2p-note"[^>]*>.*?<\/p>/';

  // Check if notice has already been inserted.
  if (preg_match($reg, $description)) {
    $msg = "P2P notice already inserted to task $tid description.";
    
    // If there are no active P2P nodes, we remove the notice.
    if (!custom_p2p_count_active_p2p($tid, $node->nid)) {
      $description = preg_replace($reg, '', $description);
      $msg = "P2P notice has been removed from task $tid description.";
    }
    
  // If the notice hasn't been inserted we add it.
  } elseif (custom_p2p_count_active_p2p($tid)) {
  
    $note  ='<p id="p2p-note" contenteditable="false" class="bs-callout bs-callout-warning"><strong>';
    $note .= t('Note: Please stick the task number @tid on the item before handing it in.', array('@tid' => $tid));
    $note .="</strong></p><br>\n\r";
    $description = $note . $description;
    $msg = "P2P notice has been inserted to task $tid description.";
  }
  
  $task->field_description['und'][0]['value']  =  $description;
  $task->field_description['und'][0]['format'] = 'filtered_html';
  $w_task->save();
  watchdog('p2p', $msg);
}


/**
 * hook_form_FORM_ID_alter
 */
function custom_p2p_form_p2p_node_form_alter(&$form, &$form_state, $form_id) {
    
  // We do not need a title for this entity. So we put in a default value
  // and hide the field on the edit form.
  $form['title_field']['#access'] = false;
  $form['field_barcode']['#access'] = false;
  $form['field_p2p_notified']['#disabled'] = true;
  
  drupal_set_title("P2P reference");
  
  // Disable task_reference field on p2p edit forms as changing the task
  // reference can mess up things.
  if (strpos($form['#action'], '/edit') !== false) {
    $page = 'edit';
    $form['field_task_reference']['#disabled'] = true;
    $form['field_task_reference']['und'][0]['target_id']['#description'] = "<span class='text-warning'> " . t('The task reference can\'t be changed on an exisiting P2P node. Create a new one instead and delete this one or set it\'s status to ›canceled‹, if not needed anymore.') . "</span>";
  }
  $form['field_mission_reference']['#disabled'] = true;
}


/**
 * hook_form_FORM_ID_alter
 */
function custom_p2p_form_commerce_product_ui_product_form_alter(&$form, &$form_state, $form_id) {
  
  // Disable stock field (demand) on edit forms if a P2P reference is referencing
  // the task currently being edited.
  if (isset($form_state['commerce_product']->product_id)) {
    
    // Get all active P2P references.
    $p2p_count = custom_p2p_count_active_p2p($form_state['commerce_product']->product_id);
    
    // If there is an active P2P node referencing the task, the demand field is
    // managed programmatically and thus we disable it for user iiput.
    if ($p2p_count) {
      $form['commerce_stock']['und'][0]['value']['#suffix'] = 
                "<span class='text-warning voffset3'>" 
                . t('This task is referenced by <strong>@number</strong> active P2P node(s) and thus it\'s demand is managed programmatically.',
                    array('@number' => $p2p_count)) 
                . "</span>";
                
      $form['commerce_stock']['#disabled'] = true;
    }
  }
  
}

/**
 * Returns count of all P2P nodes of status 'requested' and 'delivered'
 * referencing a specific task.
 *
 * @param int $tid
 *   The ID of a task (commerce product).
 *
 * @param int $nid
 *   A node ID to be excluded: used for on node delete hook, as node will be 
 *   deleted afterwards.
 *
 * @param array $field_status
 *   Array of status set in field_status.
 */
function custom_p2p_count_active_p2p($tid, $nid = 0, $field_status = array('requested', 'delivered')) {
  $query = new EntityFieldQuery();
  return $query
  ->entityCondition('entity_type', "node")
  ->entityCondition('bundle', 'p2p')
  ->entityCondition('entity_id', $nid, '!=')
  ->fieldCondition('field_task_reference', 'target_id', $tid)
  ->fieldCondition('field_status', 'value', $field_status, 'IN')
  ->count()
  ->execute();
}

/**
 * Implements hook_views_pre_view()
 */
function custom_p2p_views_pre_view(&$view, &$display_id, &$args) {
  
  // The task_reference view is used as entity selection mode to select
  // tasks from P2P nodes in an entity reference field.
  // Tasks shown there have to be drilled down to the initiaitve tasks
  // (og group) given by ID as query parameter. But we can't pass the ID as 
  // token nor get it directly from the URL, as all query parameters 
  // plus the orignal URL get lost during the AJAX call.
  // So we get the initiaitve ID (group ID) here from the referer URL and
  // passing it to the view as argument.
  if ($view->name == 'task_reference') {
    parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $query);
    $view->args[0] = isset($query['og_group_ref']) ? $query['og_group_ref'] : false;
  }
}

/**
 * Implements hook_og_permission().
 */
function custom_p2p_og_permission() {
  return array(
    "p2p send pickup notification" => array(
      'title' => t('Send P2P pick up notification'),
      'description' => t('Grant user permission to send P2P pickup notifications.'),
      'roles' => array(OG_AUTHENTICATED_ROLE, OG_ADMINISTRATOR_ROLE),
      'default role' => array(OG_ADMINISTRATOR_ROLE),
    ),                              
  );
}
