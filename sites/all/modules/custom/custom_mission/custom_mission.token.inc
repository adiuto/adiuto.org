<?php
/**
 * Implements hook_token_info().
 */
function custom_mission_token_info() {
  $info['tokens']['site']['base-url'] = array(
    'name' => t('Absolute URL'),
    'description' => t('Url of the site, no language prefix garanted'),
  );
  return $info;
}

/**
 * Implements hook_tokens().
 */
function custom_mission_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  global $base_root;

  if ($type == 'site') {

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'base-url':
          $replacements[$original] = $base_root;
          break;
      }
    }
  }
  return $replacements;
}
