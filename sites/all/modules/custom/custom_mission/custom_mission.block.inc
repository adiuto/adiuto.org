<?php
/**
 * Implements hook_block_info().
 */
function custom_mission_block_info() {
  $blocks['custom_mission_mission_planning'] = array(
    'info' => t('Custom: Mission planning'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  $blocks['custom_mission_mission'] = array(
    'info' => t('Custom: Mission block'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_mission_abort_mission'] = array(
    'info' => t('Custom: Shows form to abort a mission'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['custom_mission_steps'] = array(
    'info' => t('Custom: Show steps for mission forms'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_mission_block_view($delta = '') {
  $block = array();
  switch ($delta) {
      case 'custom_mission_mission_planning':
        $block['subject'] = t('Menu');
        $block['content'] = custom_mission_mission_planning();
        break;   
      case 'custom_mission_mission':
        $block['subject'] = t('Mission');
        $block['content'] = custom_mission_mission();
        break;
      case 'custom_mission_abort_mission':
        $block['subject'] = "";
        $block['content'] = custom_mission_abort_mission_block();
        break;
      case 'custom_mission_steps':
        $block['subject'] = "";
        $block['content'] = custom_mission_steps();
        break;
  }
  
  return $block;
}

/***
 * Show Message on mission planning
 */
function custom_mission_mission_planning() {

}



/**
 * Returns markup for a custom mission (commerce cart) block
 */
function custom_mission_mission() {
  $markup = '<div class="panel panel-default"><div class="panel-heading"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> ' . t('Mission') . '</div>';
  $markup .= '<div class="panel-body">';

  $view = views_get_view('commerce_cart_block'); //Shopping cart block
  // Preview has to be called before results, otherwise there would be no results anyway.
  $cart = $view->preview();
  #dsm($view->header);
  if ($view->result) {
    $options = array('html' => TRUE, 'attributes' => array('class' => 'btn btn-warning btn-sm btn-block'));
    $markup .= l(t('Go to mission'), '/cart', $options);
    $markup .= '<br>';
    #$markup .= ' <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span> ' . $view->result[0]->node_og_membership_title . '';
    $markup .= $cart;
    //Find More
    $options = array('html' => TRUE, 'attributes' => array('class' => array('text-break', 'btn', 'btn-link')));
    #$title 	= t('Find more tasks for') .'<br>' . $view->result[0]->field_field_location[0]['rendered']['#label'];
    $title = t('show tasks for this location');
    $id 		= $view->result[0]->field_field_location[0]['rendered']['#item']['target_id'];
    $markup .= l($title, '/node/' . $id, $options);
    #$markup .= '<br>';
    //Checkout and Edit
    #$options = array('html' => TRUE, 'attributes' => array('class' => 'btn btn-warning btn-sm btn-block '));
    #$buttons = l(t('Checkout'), '/checkout', $options) . l(t('Edit mission'), '/cart', $options);
    #$markup .= '<div class="btn-group btn-group-justified">' . $buttons . '</div>';
    #$markup .= l(t('Go to mission'), '/cart', $options);
  }
  else {
    $markup .= t('Add tasks to start a mission.');
  }
  $markup .= '</div></div>';
  return $markup;
}




/**
 * Block content callback to show form to enable user to set a custom position
 */
function custom_mission_abort_mission_block() {
  $form = drupal_get_form('custom_mission_abort_mission_form');
  return $form;
}

/**
 * Mission abort block
 */
function custom_mission_abort_mission_form($form, &$form_state) {
  
  global $user;
  $order = entity_load_single('commerce_order', arg(1));
  $form_state['entity'] = $order;
  $form_state['destination'] = drupal_get_destination();
  $form_state['user'] = $user;
  
  // If we cant load an order, abort.
  if (!isset($order->status) ) {
    return;
  }
  
  // Get human readable name of status
  $status_title = commerce_order_status_get_title($order->status);
  $status_title = !empty($status_title) ? $status_title : t("unknown");
  
  // Status to css class
  $classes = array('pending' => 'info', 'canceled' => 'danger', 'completed' => 'success');
  $class = isset($classes[$order->status]) ? $classes[$order->status] : "default";
  
  $form['abort_mission'] = array(
    '#prefix' => t("Mission status") . "<div class='voffset2 panel panel-default'>",
    '#suffix' => '</div><br />',
  );
  $form['abort_mission']['status']['#prefix'] = "<div class='panel-body text-center bg-$class'>";
  $form['abort_mission']['status']['#markup'] = '<strong>' . $status_title . '</strong>';
  $form['abort_mission']['status']['#suffix'] = '</div>';
  
  // Return markup if status is not pending (canceled, completed)
  if ($order->status != "pending") {
    return $form;
  }
  
  $gid   = $order->og_group_ref['und'][0]['target_id'];
  
  // If user is owner and has the permissions to edit own tasks
  if (!($user->uid == $order->uid && og_user_access('node', $gid, "cancel own mission"))
      && !og_user_access('node', $gid, "cancel any mission")) {
      return;
  }

  // Create input group
  $form['abort_mission']['#prefix'] = t("Mission status") . '<div class="voffset2 input-group">';
  #$form['abort_mission']['#suffix'] = '</div><br /><br />'; // Same as above
  // Put status (above) in button group addon
  $form['abort_mission']['status']['#prefix'] = "<div class='input-group-addon bg-$class'>";
  $form['abort_mission']['status']['#suffix'] = '</div>';
  // input group button
  $form['abort_mission']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Abort mission'),
    '#submit' => array('custom_mission_abort_mission_form_submit'),
    '#validate' => array('custom_mission_abort_mission_form_validate'),
    #'#disabled' => true,
    #'#attributes' => array('class' => array('input-group-btn')),
    '#prefix' => '<div class="input-group-btn">',
    '#suffix' => '</div>',
  );

  // Construct form only if we are not in the confirmation loop  
  if (!isset($form_state['confirm'])) {  
    #$form['abort_mission']['submit']['#disabled'] = false;
    return $form;
  
  // Return confirm form, if we are in the confirmation loop
  } else {
    return custom_mission_abort_mission_form_confirm($form_state);
  }
}

/**
 * Returns the drupal confirmation form
 */
function custom_mission_abort_mission_form_confirm(&$form_state) {

  $order = $form_state['entity'];
  $user = $form_state['user'];
  $path = "user/$user->uid/missions";  
  $title = t('Are you sure you want to abort %mission?', array('%mission' => drupal_get_title()));

  // If a user cancels his own mission he gets a different message as if a user
  // cancels a mission owned by somebody else.
  if ($user->uid == $order->uid) {
    $text  = "<div class='bs-callout bs-callout-warning'>" . t('We would like to kindly remind you, that you have pledged to complete this mission. Do you want to abort it anyway?') . " " . t('This action cannot be undone.') . "</div>";
  } else {
    $text  = "<div class='bs-callout bs-callout-warning'>" . t('Please note, that it is critical to abort a mission of an other user. He might have started to work on it\'s tasks already. Do you want to abort it anyway?') . " " . t('This action cannot be undone.') . "</div>";
  }
  
  // Tell the submit handler to process the form
  $form_state['confirm'] = true;
  $form['#submit'] = array('custom_mission_abort_mission_form_submit');
  $form['#suffix'] = '</br></br>';
  return confirm_form($form, $title, $path, $text);
}

/**
 * Validate handler for abort mission form
 */
function custom_mission_abort_mission_form_validate($form, &$form_state) {
  $user = $form_state['user'];
  $order = $form_state['entity'];
  $gid   = $order->og_group_ref['und'][0]['target_id'];
  
  // Mission can only be aborted when it has the status "pending"
  if ($order->status != "pending") {
    form_set_error('', t('This mission has the status %status and as such it can\'t be aborted.',
                         array('%status' => $order->status)));
  }
  
  // If user is owner and has the permissions to edit own tasks
  if ($user->uid == $order->uid) {
    if (og_user_access('node', $gid, "cancel own mission")) {
      return;
    }
  }
  
  // If user has the permission to edit any mission
  if (og_user_access('node', $gid, "cancel any mission")) {
    return;
  }
    
  form_set_error('', t('You don\'t have permissions to cancel this mission!'));
}

/**
 * Submit handler for abort mission form
 */
function custom_mission_abort_mission_form_submit($form, &$form_state) {
  if (!isset($form_state['confirm']) || !$form_state['confirm']) {
    $form_state['confirm'] = true;
    $form_state['rebuild'] = true;
  } else {
    // Load order
    $order = $form_state['entity'];
    $user = $form_state['user'];
    
    // Update mission status
    commerce_order_status_update($order, "canceled");
    $path = "user/$user->uid/missions";      
    drupal_set_message(t('Mission has been canceled.'), "status");
    
    // Go back to view: Form destination does not work, as we are in a block.
    //drupal_goto($path);
  }
}

function custom_mission_steps() {
  $markup = '';
  $steps = array(
    '<span class="label label-default">' . t('Planning') . '</span>',
    '<span class="label label-default">' . t('Checkout') . '</span>',
    '<span class="label label-default">' . t('Pending') . '</span>',
    '<span class="label label-default">' . t('Completed') . '</span>',
  );
  if (arg(0) == 'cart') {
    $step = 0;
  }
  elseif (arg(0) == 'checkout' && !arg(2)) {
    $step = 1;
  }
  elseif (arg(0) == 'checkout' && arg(2) == 'complete') {
    $step = 2;
  }
  else {
    return $markup;
  }
  $steps[$step] = str_replace('default', 'success', $steps[$step]);
  $markup = implode(' <span class="small glyphicon glyphicon-arrow-right" aria-hidden="true"></span> ', $steps);
  $markup = '<div class="checkout-steps">' . $markup . '</div>';
  return $markup;
}