<?php
/**
 * Implements hook_block_info().
 */
function custom_permissions_block_info() {
  $blocks['custom_permissions_help'] = array(
    'info' => t('Custom permissions: Help'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function custom_permissions_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'custom_permissions_help':
      $block['subject'] = "";
      $block['content'] = custom_permissions_help_block();
      break;
  }
  return $block;
}


function custom_permissions_help_block() {
  $pid = arg(1);

  if (!$pid) {
    return;
  }
  
  $entity = entity_load_single('commerce_product', $pid);

  if (!$entity || $entity->type != "product") {
    return;
  }
  
  // Get entities' groupid
  $gid = isset($entity->og_group_ref['und'][0]['target_id']) ? 
               $entity->og_group_ref['und'][0]['target_id'] : 0;

  // Load user
  global $user;
  
  if (!custom_permissions_task_edit_access_callback($pid) && $user->uid == $entity->uid) {
    
    $msg = t("Why can't I edit or delete my own tasks?");
    if (function_exists("custom_help_link")) {
      $link = custom_help_link("initiative-administration#members", 'btn-link', $msg);
    }
    $markup = "<div class='bs-callout bs-callout-info'>$link</div>";
    return $markup;
  }

}
