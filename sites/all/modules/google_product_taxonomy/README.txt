
DESCRIPTION
-----------

Google Product Taxonomy module provides the following features:

* new 'Google Product Taxonomy' vocabulary
* 2 new field and field instances for this vocabulary:
  - google_product_taxonomy_id
  - google_product_taxonomy_path
* admin page to import hierarchical Google Product categories into this
  vocabulary (and to delete them if necessary) using Drupal batch functionality

This vocabulary could be later used with Drupal Commerce
(https://www.drupal.org/project/commerce) or any other similar solution to add
Google Product Taxonomy (https://support.google.com/merchants/answer/1705911)
fields to product entities, to facilitate their export to Google Merchant
(https://support.google.com/merchants/answer/188494)
in 'google_product_category' element.


INSTALLATION
------------

1. Enable the module.
2. Go to Administration » Structure » Google Product Taxonomy
   (admin/structure/google-product-taxonomy) and import the taxonomy.
3. Add a 'Term reference' field to your 'Product' entity type (or anywhere else
   where it's needed), selecting 'Google Product Taxonomy' as the source
   vocabulary.


RECOMMENDED MODULES
-------------------

* Simple hierarchical select (https://www.drupal.org/project/shs)
  to facilitate selecting Google product categories from Google Product
  Taxonomy hierarchical vocabulary.

